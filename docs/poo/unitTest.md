# Pruebas unitarias

Las pruebas unitarias corresponden a la ejecución automatica de código que permita chequear si cumple con lo esperado.

Una Forma de hacer esto es con el framework **JUNIT** 

## Uso JUnit5

Dentro de un proyecto **maven**, [JUnit5](https://junit.org/junit5/docs/current/user-guide/) es necesario integrarlo en el archivo "pom.xml" dentro del tag de ````<dependencies>````, de la siguiente forma.

````xml
<dependency>
    <groupId>org.junit.jupiter</groupId>
    <artifactId>junit-jupiter</artifactId>
    <version>5.8.2</version>  <!--la versión puede variar-->
</dependency>
````

## Ciclo de vida

![junitlife](junitlife.png)

####  SetUP

Corresponde al código necesario para poder lanzar un grupo de pruebas y pruebas de forma particular

#### Test

Corresponde al código necesario para realizar una prueba unitaria.

#### TearDown

Corresponde al código necesario para limpiar, desmontar, eliminar para cada prueba, como para cada grupo de pruebas

### Anotaciones 

![junitAnotation](junitanotation.png)

Las anotaciones permiten utilizar los métodos en configuraciones tales que permitan realizar los test en varias formas.

#### Anotaciones Basicas

##### @BeforeAll
Esta anotación se utiliza exclusivamente con métodos estaticos. Los métodos con esta anotación se ejecutan antes que cualquier otro método de la clase.

##### @BeforeEach
Esta anotación se ejecuta antes de cada *test*, como por ejemplo los @Test, @ParametrizedTest, @RepeatedTest, etc.

##### @AfterEach
Esta anotación se utiliza exclusivamente con métodos estaticos. Los métodos con esta anotación se ejecutan despues de todas los *test* de la clase.


##### @AfterAll
Esta anotación se ejecuta despues de cada *test*, como por ejemplo los @Test, @ParametrizedTest, @RepeatedTest, etc.

##### @Test
Esta anotación es la que permite realizar los test de una clase determinada. Los bétodos deben ser exclusivamente de tipo ```public void```

#### Anotaciones Complementarias

Estas anotaciones funcionan de forma complementaria a los "@test" de forma que se pueden aplicar test con nuevas configuraciones.

##### @ParameterizedTest
Esta anotación permite aplicar un test, con distintos valores parametrizados mediante un csv, un array, un enum,etc.

##### @RepeatedTest
Esta anotación permite ejecutar un test y repetirlo un número definido de veces.

##### @Nested
Esta anotación permite agrupar una serie de test segun algun tópico.

#### Otras anotaciones
Las anotaciones anteriormente mencionadas permiten realizar gran parte de las formas de test, sin embargo, existen unagran cantidad de anotaciones que permiten configurar y adaptar los test aun más, todas las anotaciones se encuentran disponibles directamente en la documentación de [jUnit5](https://junit.org/junit5/docs/current/user-guide/#writing-tests-annotations). A continuación se presentan algunos ejemplos.

##### @Disabled
Esta anotación sirve para poder deshabilitar un determinado test. De modo que al momento de realizar los test para una clase determinada se visualizen no solo los test correctos o incorrectos, si no que tambien se visualicen los test omitidos.

##### @Tag
Esta anotación permite añadir etiquetas para eventualmente ejecutar test solo los test con un *tag* determinado.

##### @DisplayName
Esta anotación permite que un test al ser ejecutado despliegue el un titulo determinado dentro de la lista de pruebas ejecutadas.
