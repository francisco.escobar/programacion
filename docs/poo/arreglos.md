# Arreglos y matrices

Usamos un arreglo para procesar una colección de datos del mismo tipo, como una lista de temperaturas
registradas durante un día o una lista de nombres obtenida desde un archivo de alumnos de un curso. En
este capítulo te presentaremos los fundamentos de la definición y uso de arreglos en el lenguaje Java, y muchas de las técnicas básicas que se emplean para diseñar algoritmos y programas que usan arreglos.

## Arreglos

Un arreglo es una zona de memoria (variable) que puede almacenar un conjunto de N datos del mismo tipo.

![arr](Arreglo.png)

### Propiedades de un arreglo

- **Largo de Arreglo**: Corresponde al numero de elementos que compone el arreglo. Este numero SIEMPRE es un número entero.
![arr](Largo.png)


- **Indices**: Representa la posición de los elementos dentro del arreglo. Este comienza en 0 y termina en el indice "n-1".

![arr](indice.png)

- **Contenido**: Corresponde a los elementos almacenados en un espacio especifico.

- **Dirección**: Corresponde al dato que corresponde a la dirección de referencia del arreglo.

```java
    int[] arr = new int[10];
    System.out.println(arr);

    //salida
    [I@5a39699c

```

**Nota**
No confundir el inice con el dato almacenado.

## Arreglos Unidimencionales

### Declaración de arreglos

Para declarar un nuevo arreglo, es necesario tener claro el tipo de veriable, el nombre del arreglo y el largo del arreglo.

``` java
tipo_variable[] nombre_arreglo = new tipo_variable[largo];
```
#### ejemplos

Declaración simple

=== "int"
    ````java
    int[] temperatura = new int[10];
    ````

=== "char"
    ````java
    char[] vocales = new char[5];
    ````

=== "String"
    ````java
    String[] palabras = new String[10];

    ````

Declaración con largo definido por una variable

=== "int"
    ````java
    int n = 12;
    int[] temperatura = new int[n];
    ````

=== "char"
    ````java
    int n = 15;
    char[] vocales = new char[n];
    ````

=== "String"
    ````java
    int n = 5;
    String[] palabras = new String[5];

    ````


### Acceso a un arreglo

El acceso a un arreglo se realiza mediante el nombre del arreglo y el indice donde esta ubicado el dato.

#### Acceso simple

=== "int"
    ````java
    temperatura[2]
    ````

=== "char"
    ````java
    vocales[1]
    ````

=== "String"
    ````java
    palabras[3]
    ````

#### Acceso mediante una variable
=== "int"
    ````java
    int var = 2;
    temperatura[var];
    ````

=== "char"
    ````java
    int var = 5;
    vocales[var]
    ````

=== "String"
    ````java
    int var = 2;
    palabras[var]
    ````


### Obtener el largo del arreglo

````java
int n = 12;
int[] temperatura = new int[n];

// Obtener el largo del arreglo
temperatura.length; // El resultado sera igual a n.

````

### Inicializar un arreglo

**Declaración de arreglo con datos.**
=== "int"
    ````java
    int[] temperatura = {1, 45, 23, 34, 900, 34} ;
    ````

=== "char"
    ````java
    char[] caracteres = {'a', 'á', 'e', '1'} ;
    ````

=== "String"
    ````java
    String[] mamiferos = {"perro", "gato", "conejo","oso"};
    ````

**Llenar un arreglo**
=== "int"
    ````java
    temperatura[0] = 12;
    temperatura[1] = 14;
    temperatura[2] = 19;
    temperatura[3] = 16;
    temperatura[4] = 20;
    ````

=== "char"
    ````java
    vocales[0] = 'e';
    vocales[1] = 'a';
    vocales[2] = 'í';
    vocales[3] = 'o';
    vocales[4] = 'Ü';
    ````

=== "String"
    ````java
    palabras[0] = "caja";
    palabras[1] = "botella";
    palabras[2] = "carton";
    palabras[3] = "agua";
    palabras[4] = "papel";
    ````


### Uso de ciclos con arreglos

#### Mostrar arreglo
=== "int"
    ````java
    for(int i=0;i<temperatura.length;i++){
        System.out.println( temperatura[i] );
    }
    ````

=== "char"
    ````java
    for(int i=0;i<vocales.length;i++){
        System.out.println( vocales[i] );
    }

    ````

=== "String"
    ````java
    for(int i=0;i<palabras.length;i++){
        System.out.println( palabras[i] );
    }
    ````


#### Llenar arreglo
=== "int"
    ````java
    // import java.util.Scanner;
    Scanner teclado = new Scanner();

    for(int i=0;i<temperatura.length;i++){
        temperatura[i]=teclado.nextInt();
    }
    ````

=== "char"
    ````java
    // import java.util.Scanner;
    Scanner teclado = new Scanner();

    for(int i=0;i<vocales.length;i++){
        vocales[i]=teclado.next().charAt(0);
    }

    ````

=== "String"
    ````java
    // import java.util.Scanner;
    Scanner teclado = new Scanner();

    for(int i=0;i<palabras.length;i++){
        palabras[i] = teclado.next();  
    }
    ````

## Arreglos Multidimencionales

A veces es útil disponer de un arreglo con más de un índice, ya que nos da la posibilidad de manipular información que requiere dos o más dimensiones.

Por ejemplo, para el registro de notas para un curso, se necesitarán dos dimensiones. Una para los alumnos y la otra dimensión de las evaluaciones registradas, donde el contenido de la matriz serian las notas.

![mat](matriz.png)


### Declaración de matrices

Para declarar una nueva matriz, es necesario tener claro el tipo de veriable, el nombre del arreglo y las dimensiones de la matriz.

```` java
tipo_variable[][] nombre_matriz = new tipo_variable[largo][ancho];
````
#### ejemplos

Declaración simple

=== "int"
    ````java
    int[][] temperatura = new int[10][15];
    ````

=== "char"
    ````java
    char[][] vocales = new char[5][3];
    ````


=== "String" 
    ````java
    String[][] palabras = new String[10][4];
    ````


Declaración con largo definido por una variable

=== "int"
    ````java
    int dim1 = 12;
    int dim2 = 8;
    int[][] temperatura = new int[dim1][dim2];
    ````

=== "char"
    ````java
    int dim1 = 2;
    int dim2 = 5;
    char[][] vocales = new char[dim1][dim2];
    ````

=== "String"
    ````java
    int dim1 = 24;
    int dim2 = 8;
    String[][] palabras = new String[dim1][dim2];

    ````

#### Acceso simple

=== "int"
    ````java
    temperatura[2][1]
    ````

=== "char"
    ````java
    vocales[1][9]
    ````

=== "String"
    ````java
    palabras[3][5]
    ````

#### Acceso mediante una variable
=== "int"
    ````java
    int var0 = 2;
    int var1 = 6;
    temperatura[var0][var1];
    ````

=== "char"
    ````java
    int var0 = 7;
    int var1 = 3;
    vocales[var0][var1];
    ````

=== "String"
    ````java
    int var0 = 8;
    int var1 = 2;
    palabras[var0][var1];
    ````


### Obtener el largo del arreglo

````java
int dim1 = 2;
int dim2 = 5;
char[][] vocales = new char[dim1][dim2];

// Obtener las dimensiones de la matriz
vocales.length;     // Mostrará el largo de una dimensión (dim1)
vocales.length[2];  // Mostrará el largo de una de las filas (dim2)
````

### Inicializar un arreglo

**Declaración de matriz con datos.**

=== "int"
    ````java
    int[][] temperatura = {{1, 45, 23, 34},{2,45},{21,43,9}};
    ````

=== "char"
    ````java
    char[][] caracteres = {{'a', 'á'},{'s','e','w'},{'2','0'},{'+','|','°','&'}} ;
    ````

=== "String"
    ````java
    String[][] mamiferos = {{"perro", "gato", "conejo","oso"},{"elefante", "raton"},{"lobo", "leon", "tigre"}};

    ````

**Llenar un arreglo**

=== "int"
    ````java
    temperatura[0][5] = 12;
    temperatura[0][6] = 14;
    temperatura[2][4] = 19;
    temperatura[2][3] = 16;
    temperatura[1][0] = 20;
    ````

=== "char"
    ````java
    vocales[0][2] = 'e';
    vocales[0][1] = 'a';
    vocales[1][9] = 'í';
    vocales[1][2] = 'o';
    vocales[4][3] = 'Ü';
    ````

=== "String"
    ````java
    palabras[1][3] = "caja";
    palabras[1][2] = "botella";
    palabras[3][3] = "agua";
    palabras[2][9] = "carton";
    palabras[4][1] = "papel";
    ````


### Uso de ciclos con arreglos

#### Mostrar arreglo
=== "int"
    ````java
    for(int i=0;i<temperatura.length;i++){
        for(int j=0;j<temperatura[i];j++){
            Sysytem.out.print(temperatura[i][j]);
        }
        System.out.println();
    }
    ````

=== "char"
    ````java
    for(int i=0;i<vocales.length;i++){
        for(int j=0;j<vocales[i];j++){
            Sysytem.out.print(vocales[i][j]);
        }
        System.out.println();
    }

    ````

=== "String"
    ````java
    for(int i=0;i<palabras.length;i++){
        for(int j=0;j<palabras[i];j++){
            Sysytem.out.print(palabras[i][j]);
        }
        System.out.println();
    }
    ````


#### Llenar arreglo
=== "int"
    ````java
    // import java.util.Scanner;
    Scanner teclado = new Scanner();

    for(int i=0;i<temperatura.length;i++){
        for(int j=0;j<temperatura.length;j++){
            temperatura[i][j]=teclado.nextInt();  
        }
    }
    ````

=== "char"
    ````java
    // import java.util.Scanner;
    Scanner teclado = new Scanner();

    for(int i=0;i<vocales.length;i++){
        for(int j=0;j<vocales.length;j++){
            vocales[i][j]=teclado.next().charAt(0);  
        }
    }

    ````

=== "String"
    ````java
    // import java.util.Scanner;
    Scanner teclado = new Scanner();

    for(int i=0;i<palabras.length;i++){
        for(int j=0;j<palabras.length;j++){
            palabras[i][j] = teclado.next();  
        }  
    }
    ````

## Cadenas

Una cadena es un vector que almacena N caracteres, por lo tanto cada celda puede almacenar un dato de tipo char.

![cadena](cadena.png)

En Java las cadenas son objetos de las clases predefinida String o StringBuffer, que están incluidas en el paquete java.lang.*

**String**: Java crea un arreglo de caracteres o una cadena. A estas cadenas accedemos a través de las funciones que poseen esta clase.

### Declaración y ejemplos de cadenas

```` java
char[] palabra0 = new char[10];
char[] caracteres = {'a', 'á', 'e', '1'} ;

String palabra1 = "hola, mundo!";
char[] palabra2 = "String a  char".toCharArray();  // convertir String a arreglo de caracteres

String palabras = "Una cadena es un vector que almacena N caracteres";
char[] palabrasChar = palabras.toCharArray();     // convertir String a arreglo de caracteres

String frase="";            // Ejemplos de String nulo
String frase=new String();  // Ejemplos de String nulo


char data[] = {'a', 'b', 'c'};
String str = new String(data); //crear un String a partir de un arreglo de char
````

### Funciones propias para **String**

```` java
String c1 = " perro ";
String c2 = "gato";

````

|  Funciones    |  Descripción  |  Ejemplo |  Resultado|
| ------------- |:-------------:|----------|----------:|
|```.concat(String)```| Concatena las cadenas. |```c1.concat(c2); ```|```" perro gato"```|
|```.equals(String)```| Comprueba si las cadenas son iguales. |```c1.equals(c2);```|```false```|
|```.trim()```        | Elimina los espacios del principio y fin de la cadena.|```c1.trim();```|```"perro"``` |
|```.length()```      | Devuelve la cantidad de caracteres del String.|```c2.length();```|```6```|


## ArrayList
Corresponde a un objeto que representa un arreglo de largo variable. El cual posee metodo para realizar variadas operaciones como:

1. añadir elementos al arreglo en un indice especifico o al final
2. quitar elementos de un arreglo, segun su indice o su contenido.
3. crear arreglos de cualquier objeto.
4. etc.

### Declarar un ArrayList

1. Es necesario implementar una libreria (```java.util.ArrayList```)
2. Solo utiliza clases contenedoras para trabajar con tipos de datos primitivos
2.

### Ejemplo

````java
// importar libreria
import java.util.ArrayList;


// crear objeto
ArrayList<String> palabras = new ArrayList<>();

// añadir elementos
palabras.add("Antonela");
palabras.add("Patricio");
palabras.add("Daniel");
palabras.add("Andres");

// eliminar elemento por Indices
palabras.remove(0);

// eliminar elemento por objeto Contenido
palabras.remove("Daniel");

// añadir objeto según indice
palabras.add(1,"Pamela");

// remplazar objeto
palabras.set(2,"Javiera");

// verificar si existe un objeto
palabras.contains("Javiera");

// vaciar arreglo
palabras.clear();

// Obtener un elemento del arreglo
String p = palabras.get(1);

````
