# Clases Contenedoras

Estas clases permiten utilizar los tipos de datos primitivos como objetos. 

## Usos

### Uso como Genericos

Es posible usas las clases contenedoras para crear objetos de clases que utilizan "genericos" como la clase ````ArrayList<E>````.

=== "ArrayList"

    ````java
    ArrayList<Integer> arreglo = new ArrayList<>();

    ````
=== "ArrayList no valido"
    ````java
    ArrayList<int> arreglo = new ArrayList<>();

    ````

### Funciones

Es posible usar las funciones de estas clases para obtener convertir tipos de datos, realizar comparaciones u otras operaciones, Estas funciones pueden variar entre las clases contenedoras.


### Tipos disponibles

|Primitivo |Wrapper Class|
|----|----|
|byte 	|Byte|
|short 	|Short|
|int 	|Integer|
|long 	|Long|
|float 	|Float|
|double 	|Double|
|boolean 	|Boolean|
|char 	|Character|


