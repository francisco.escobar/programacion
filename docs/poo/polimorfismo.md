# Polimorfismo

Los polimorfismos permiten a los objetos de una clase determinada, que pertenece a una familia de clases relacionadas por herencia, pueda ser utilizado como referencia a otra clase.

![ejemploHerencia](herenciaParaPolimorfismo.png)

### Ejemplo polimorfismo

###### Ejemplo de herencia

=== "Animal.java"
    ```java
    public class Animal {
        public void hacerSonido(){
            System.out.println("...");
        }
    }
    ```

=== "Perro.java"
    ```java
    public class Perro extends Animal{
        @Override
        public void hacerSonido(){
            System.out.println("Ladrando, ladrando, ladrando");
        }
    }
    ```
=== "Gato.java"
    ```java
    public class Gato extends Animal {
        @Override
        public void hacerSonido(){
            System.out.println("Maullando, maullando, maullando");
        }
    }
    ```


###### Clase principal

En esta clase se procede a realizar llamados mediante polimorfismo.

=== "Principal.java"
    ```java

    public class Main {
        public static void main(String[] args) {
            Animal animal = new Animal();  //instancia simple de animal

            Animal perro = new Perro(); // instancia de Perro como polimorfismo
            Animal gato = new Gato();   //instancia de Gato como polimorfismo

            Perro perro1 = new Perro();
            Gato gato1 = new Gato();
            
            ejecutarSonido(animal); //uso simple del objeto animal
            ejecutarSonido(perro); //Uso polimorfismo en el llamado a una función
            ejecutarSonido(gato);  //Uso polimorfismo en el llamado a una función
            ejecutarSonido(perro1);//Uso polimorfismo en el llamado a una función
            ejecutarSonido(gato1); //Uso polimorfismo en el llamado a una función
        }

        private static void ejecutarSonido(Animal animal) {
            animal.hacerSonido();
        }
    }
    ```

=== "Salida"
    ```
    ...
    Ladrando, ladrando, ladrando
    Maullando, maullando, maullando
    Ladrando, ladrando, ladrando
    Maullando, maullando, maullando

    ```