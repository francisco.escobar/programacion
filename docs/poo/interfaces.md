# Interfaces

- Las interfaces definen una forma estándar y pública de especificar el comportamiento de clases.
- Los métodos definidos dentro de una interfaz son abstractos
- Todo los elementos dentro de una interface son publico, por lo que no es necesario utilizar ese modificador.


#### Interfaz "Animal.java"
```java
interface Animal {
    void hacerRuido(); // El método es abstracto sin necesidad del uso de operador "abstract"
    void correr(); // los métodos abstractos no requieren de un cuerpo
}

```

> Desde java 9 es posibles utilizar métodos privados

### Interfaces extendidas
Las interfaces tambien pueden extenderse de forma similar a como funciona la herencia. 

```java
interface Mamifero extends Animal{
    //métodos de interface
}
```

#### Interfaces extendidas multiples

A diferencia de las clases, las interfaces si pueden extender a varias interfaces, de la misma forma que una clase puede implementar más de una interface.

```java
interface Mamifero extends Animal, Runnable, Serializable {
    //métodos de interface
}
```

```java
class gato implements Mamifero, Comparable{
    //
    // es obligatorio implementar los métodos de las interfaces
    //
}
```

