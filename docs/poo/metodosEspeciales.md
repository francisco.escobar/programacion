# Funciones Especiales
## Ordenamiento (Burbuja)
```java
public static void burbuja(int[] arr) {
        for (int i = 0; i < arr.length-1; i++) {
            for (int j = 0; j < arr.length-1; j++) {
                if(arr[j]<arr[j+1]){
                    int aux = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = aux;
                }
            }
        }
}

```

Uso de la función

```java
import java.util.Arrays;

public class Principal {
    public static void main(String[] args) {
        int arr[] = {7, 4, 5, 12, 34, 5, 7, 0, 4};
        burbuja(arr);
        System.out.println("arr = " + Arrays.toString(arr));

    }
}
```

Salida

```java
arr = [34, 12, 7, 7, 5, 5, 4, 4, 0]
```


## Recursividad

Comprende a la técnica de programación donde un elemento se instancia a si mismo.

En la siguiente figura se púede apreciar como se llama a una funcion con un valor y esta se llama a si misma con otro valor, donde eventualmente la condición de fin se encarga de retornar el resultado final de la función, la cual retornará en forma de cascada hasta el primer llamado dela función.

![Recursividad](recursividad.png)

### Ejemplos
#### Factorial

```java
public static void main(String[] args) {
    System.out.println("Ingresa un numero");
    Scanner teclado = new Scanner(System.in);

    long factorial = teclado.nextInt();
    factorial = factorialRecursivo(factorial);
    System.out.println("El factorial es: "+ factorial);
}

private static long factorialRecursivo(long num) {
    if(num==1){
        return 1;
    }else{
        return num*factorialRecursivo(num-1);
    }
}
```


#### Cociente

```java
public static void main(String[] args) {
    System.out.println("El cociente de 10/3 es:");
    System.out.println(cociente(10,3));
}

public static int cociente(int a, int b) {
    System.out.println("LLamada al cociente de "+a+" y "+b);
    if(a < b){
        return 0;
    }else{
        return 1+cociente(a-b,b);
    }
}


```
#### Fibbonaci

```java
public static void main(String[] args) {
    System.out.println("Ingresa un numero");
    Scanner teclado = new Scanner(System.in);

    int fib = teclado.nextInt();
    System.out.println("el ultimo numero de la serie de fibonacci es:");
    System.out.println(fibbonacciRecursivo(fib));
}

private static int fibbonacciRecursivo(int num) {
    if(num<=1){
        return num;
    }else{
        return fibbonacciRecursivo(num-1)+fibbonacciRecursivo(num-2);
    }
}

```

#### MergeSort


```java
import java.util.Arrays;


  	public static void main(String[] args)
  	{
    		Integer[] a = {2, 6, 3, 5, 1};
    		mergeSort(a);
    		System.out.println(Arrays.toString(a));
  	}

  	public static void mergeSort(Comparable [ ] a)
  	{
    		Comparable[] tmp = new Comparable[a.length];
    		mergeSort(a, tmp,  0,  a.length - 1);
  	}


    private static void mergeSort(Comparable [ ] a, Comparable [ ] tmp, int izquierda, int derecha)
    {
        if( izquierda < derecha )
        {
          	int center = (izquierda + derecha) / 2;
          	mergeSort(a, tmp, izquierda, center);
          	mergeSort(a, tmp, center + 1, derecha);
          	merge(a, tmp, izquierda, center + 1, derecha);
        }
    }


    private static void merge(Comparable[ ] a, Comparable[ ] tmp, int izquierda, int derecha, int derechaEnd )
    {
        int izquierdaEnd = derecha - 1;
        int k = izquierda;
        int num = derechaEnd - izquierda + 1;

        while(izquierda <= izquierdaEnd && derecha <= derechaEnd)
            if(a[izquierda].compareTo(a[derecha]) <= 0)
                tmp[k++] = a[izquierda++];
            else
                tmp[k++] = a[derecha++];

        while(izquierda <= izquierdaEnd)    // Copia el resto de la primera mitad
            tmp[k++] = a[izquierda++];

        while(derecha <= derechaEnd)  // copia el resto de la mitad derecha
            tmp[k++] = a[derecha++];

        // reemplaza el valor tmp
        for(int i = 0; i < num; i++, derechaEnd--)
            a[derechaEnd] = tmp[derechaEnd];
    }


```
