# Privacidad de Elementos en Java

### Permisos
Los permisos pueden ser aplicados a clases, métodos y a atributos.
#### Public
Permite acceder desde la clase, package, subclase y el mundo.
``` java
public class Canva{

}
```

#### Protected  
Permite acceder desde la clase, package y subclase.

``` java
protected class Canva{

}
```

#### Sin Control de Acceso
Permite acceder desde la clase y package.

``` java
class Canva{

}
```

#### Private
Permite acceder solo desde la clase.
``` java
private class Canva{

}
```