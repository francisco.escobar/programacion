# Record Class

Esta es una clase especial que es útil para definiciones de clases sencillas, que no requieran más métodos que los necesarios para especificar los atributos y consultar estos.

## Caracteristicas

- extiende de ````java.lang Record````
- es una clase ````final````
- se crean métodos implicitos

## Métodos implicitos 

- Constructor
- getters (poseen el mismo nombre de los atributos, sin el prefigo get/is)
- ````toString()````
- ````equals()````
- ````hashCode()````
- ***No existen los métodos setters *para este tipo de clase**
 
=== "PersonaRecord.java"
    ````java
    public record PersonaRecord(String nombre, int edad, String email) {
    }

    ````
=== "Uso"
    ````java
    public static void main(String[] args) {
        PersonaRecord p = new PersonaRecord("Eduardo",45,"e.silva@correo.com");
    }
    ````