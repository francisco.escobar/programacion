# ENUM

Este clase especial permite almacenar constantes en ella, todos los elementos definidos en una clase ```` enum```` son de tipo ````final````, por lo que no pueden ser modificados.

#### Ejemplo


=== "DiaSemana.java"
    ````java
    enum DiaSemana{
        LUNES, 
        MARTES,
        MIERCOLES,
        JUEVES,
        VIERNES,
        SABADO,
        DOMINGO
    }
    ````

=== "uso"
    ````java
    public static void main(String[] args){
        DiaSemana d = DiaSemana.LUNES;
    }

    ````

## ENUM con atributos

Los elementos enum podrian estar compuestos por más de una caracteristica, como un atributo numerico o incluso otro string.

Para esto es necesario definir los elementos enum con los atributos entre parentesis, el o los atributos que componen el enum, un constructor que asigne los valores a los atributos y si es necesario un metodo que permita consultar los atributos de los enum creados.

````java
public enum Tiempo {
    //elementos enum
    HORA(24),
    MINUTOS(60),
    SEGUNDOS(60);

    private int cantidad;

    private Tiempo(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getCantidad() {
        return cantidad;
    }
}

````


## Uso de enum

### valueOf(String)

Esta función retorna el nombre del elemento enum.

````java
System.out.println(DiaSemana.valueOf());
````

### values()

Esta función retorna un arreglo con las constantes definidas en la clase enum.


````java
public static void main(String[] args) {
    for (DiaSemana d :DiaSemana.values()) {
        System.out.println(d);
    }
}
````

### casos en switch

Los elementos enum, pueden ser utilizados como 'casos' para la estructura de control ````switch````.


````java
public static void main(String[] args) {
    DiaSemana d = DiaSemana.LUNES;

    switch (d){
        case LUNES:
            System.out.println("Lunes");
            break;
        case MARTES:
            System.out.println("Martes");
            break;
        case MIERCOLES:
            System.out.println("Miercoles");
            break;
        case JUEVES:
            System.out.println("Jueves");
            break;
        case VIERNES:
            System.out.println("Viernes");
            break;
        case SABADO:
            System.out.println("Sabado");
            break;
        case DOMINGO:
            System.out.println("Domingo");
            break;
    }
}
````
