# UML - Unified Modeling Languaje

El lenguaje de modelado unificado (UML), es un lenguaje de modelado de sistemas de software, es decir, un lenguaje grafico que permite:
 
 - visualizar
 - representar
 - construir
 - documentar


Es posible abarcar distintos aspectos y distintos niveles de abstracción de un mismo sistema como por ejemplo:


=== "Casos de Uso"
    ![use case](useCase.png)
=== "Base de datos"
    ![BaseDatos](db.png)
=== "Componentes"
    ![component](component.png)

