# Relaciones

Existen distintos tipos de relación entre las clases y cada una representa una forma de interación del sistema.

![relaciones](relaciones.png)


## Dependencia

Corresponde a la relación de uso entre dos entidades, es decir, una usa a la otra.

### caracteristicas

- Se genera cuando una clase depende de la funcionalidad que ofrece otra clase.
- Se puede decir que una clase es *cliente* del servicio de la otra clase
- Es la relacion más básica entre clases
- Es una relacion debil 

=== "UML"
    En UML la flecha va desde la clase cliente hasta la clase que ofrece el servicio/funcionalidad

    ![dependencia](dependencia.png)

    - **A** depende de **B**
    - La clase **A** usa la clase **B**
    - La clase **A** conoce la existencia de la clase **B**, pero la clase **B** no conoce la existencia de la clase **A**
    - Cambios realizados en **B**, podrian afectar a la clase **A**

=== "Código en A"
    ````java

    // dentro de un método en A
    B objB = new B();


    //Parametro en método en la clase A

    public void metodo(b objB){
        //...
    }

    ````

=== "Código en B"
    ````java
    public class B{

    }

    ````

## Asociación

Es un tipo de realción estructural entre entidades, es decir, una entidad que se construye a partir de otras antidades.
se debe entender finalmente como la forma de construir elementos complejos a partir de otros elementos más simples.

Otra forma de decirlo es, que un objeto esta integrado por atributos que a su vez son otros objetos.


![asociacionDef](asociacionDef.png)


1. La clase **A** conoce la existencia de la clase **B**, pero la clase **B** no conoce la existencia de **A**.
2. Todo cambio realizado en **B**, podrá afectar a **A**
3. La clase **B** es parte de la Clase **A**


#### Ejemplo

=== "Diagrama"

    ![asociacion](asociacionEjemplo.png)

=== "Codigo Java"
    ````java
    public class Auto {
        private Rueda r;
        private Motor m;
        private Radio r;
        //...

        public Auto() {
            //...
        }
    }


    ````

### Asociacion a->b

=== "UML"
    ![asociacionab](asociacionab.png)

=== "A.java"

    ````java

        public class A{
            public B b;
            //...
        }
    ````

=== "B.java"
    ````java

        public class B{
            public A a;
            //...
        }
    ````


### Asociacion X->b

=== "UML"
    ![asociacionxb](asociacionxb.png)

=== "A.java"

    ````java

        public class A{
            private B b;
            //...
        }
    ````


=== "B.java"

    ````java

        public class B{
            //...
        }
    ````


### Asociacion X->b 0..*

=== "UML"
    ![asociacion0b](asociacion0b.png)
=== "A.java"

    ````java

        public class A{
            private ArrayList<B> b;
            //...
        }
    ````

=== "B.java"

    ````java

        public class B{
            //...
        }
    ````


## Asociación - Agregación

Una agregación es una asociación que representa el vinculo entre el todo y sus partes. Se representa con un rombo en el lado del “TODO” y sin el rombo En el lado de la “PARTE”

Se agregan objetos a otro objeto a travez de un método y se incluye en una estructura contenedora de objetos,  como un array.

![agregacionDef](agregacionDef.png)

#### Ejemplo


=== "Diagrama"
    "La universidad agrupa muchos alumnos"

    ![agregacionEjemplo](agregacionEjemplo.png)

=== "Código-Java"

    ````java
    import java.util.ArrayList;

    public class Universidad {  
        private ArrayList<Alumno> alumnos = new ArrayList<Alumno>();
        //...

        //...
        public void add(Alumno a){
            alumnos.add(a);
        }
    }

    ````

### Agregación a->b

=== "UML"

    ![agregacionab](agregacionab.png)

=== "A.java"

    ````java

        public class A{
            public B b;
            //...
        }
    ````

=== "B.java"

    ````java

        public class B{
            public A a;
            //...
        }
    ````

### Agregación X->b

=== "UML"

    ![agregacionxb](agregacionxb.png)

=== "A.java"

    ````java

        public class A{
            private B b;
            //...
        }
    ````

=== "B.java"

    ````java

        public class B{
            //...
        }
    ````

### Agregación X->b 0..*

=== "UML"
    ![agregacionxb0](agregacionxb0.png)

=== "A.java"

    ````java

        public class A{
            private ArrayList<B> b;
            //...
        }
    ````

=== "B.java"

    ````java

        public class B{
            //...
        }
    ````

## Asociación - Composición

Es una relación de vida, donde elementos de **B** no pueden existir sin la relación con **A**, Solo se diferencia a nivel *semantico* con agregación, a nivel de código es posible implementarlo de la misma forma.


![composicionDef](composicionDef.png)

#### Ejemplo


=== "Diagrama"
    "La clase Motor es parte de la clase Auto"

    ![composicionEjemplo](composicionEjemplo.png)

=== "Código-Java"

    ````java
    public Auto  {  
        private Motor m;
        //...

    }

    ````

### Composición a->b

=== "UML"

    ![composicion](composicionab.png)

=== "A.java"

    ````java

        public class A{
            public B b;
            //...
        }
    ````

=== "B.java"

    ````java

        public class B{
            public A a;
            //...
        }
    ````

### Composicion X->b

=== "UML"

    ![composicionxb](composicionxb.png)

=== "A.java"

    ````java

        public class A{
            private B b;
            //...
        }
    ````

=== "B.java"

    ````java

        public class B{
            //...
        }
    ````

### Composicón X->b 1..*

=== "UML"
    ![composicionxb1](composicionxb1.png)

=== "A.java"

    ````java

        public class A{
            private ArrayList<B> b;
            //...
        }
    ````

=== "B.java"

    ````java

        public class B{
            //...
        }
    ````




## Diferencia agregación y composición

=== "Agregación"

    Se requiere llevar el registro de las piezas para saber su vehiculo de origen, Las piezas se reparan y comercializan por separado.

    ![vehiculo](vehiculoAgre.png)

=== "Composición"

    Se requiere llevar el registro de algunas piezas con propositos de mantención. dar de baja un vehiculo implica tambien dar de baja sus piezas.

    ![vehiculo](vehiculoComp.png)




## Herencia - Generalización

La herencia es una relación entre clases, en la que una clase comparte la estructura y/o comportamiento definidos en una o más clases. (Booch, 1994)

La herencia entre clases consiste en una relación especial que conecta dos o más conceptos muy similares entre sí, dentro de un problema.

Permite que una clase tome todos los atributos y métodos de otra clase, esto evita tener que redefinir atributos y métodos similares en varias clases parecidas.


### Generalización UML
![herencia](herencia.png)

- La clase de la izquierda es llamada clase *hija*, subclase o clase derivada
- La clase de la derecha es llamada clase *padre*, superclase o clase base


=== "Diagrama"

    ![herenciaejemplo](generalizacionEjemplo.png)

=== "Clase Animal"

    Animal.java
    ````java
    public class Animal {
        private int numPatas;
        protected String especie;

        //...

        public void mostraNumPatas(){
            //...
        }

        //...
    }

    ````

=== "Clase Gato"

    Gato.java
    ````java
    public class Gato extends Animal{
        private String nombre;
        //...

        public void morder(){
            //...
        }

        public void escalar(){
            //...
        }
    }

        
    ````



## Implementación - Realización


Una realización es una relación entre dos entidades, donde una de ellas corresponde a una interfaz que especifica un “contrato” que otra entidad garantiza llevar a cabo mediante la implementación de los métodos especificadas en el contrato.

- Mientras que la herencia *agrupa* clases que son de la misma *familia*, la realización *agrupa* clases que *realizan las mismas operaciones*
- La realización la identificamos cuando encontramos la relación hace/realiza entre una clase y la interfaz.
- Todos los métodos de la interfaz son declaraciones
- No existe implementación
- Todos los miembros son publicos
- Todos los metodos son abstractos
- Todos los campos son static y final (util para declaración de constantes)
- No es posible crear objetos de una interfaz
- Una clase puede implementar varias *Interfaces*

### Realización UML

![Implementacion](implementacion.png)



- La clase de la izquierda implementa la Interfaz de la derecha

=== "Diagrama"

    ![interfaceEjemplo](interfaceEjemplo.png)

=== "Encendido"
    ````java
    public interface Encendido{

        void encender();
        void apagar();
    }

    ````

=== "Lampara"
    ````java
    public class Lampara implements Encendido{

        @Override
        public void encender() {

        }

        @Override
        public void apagar() {

        }
    }
    ````


=== "Motor"

    ````java
    public class Motor implements Encendido{
        @Override
        public void encender() {

        }

        @Override
        public void apagar() {

        }
    }
    ````

### Generalización de una interfaz

- Una Interfaz puede heredar metodos de otras interfaces
- Una Interfaz puede heredar metodos de varias interfaces 

=== "Herencia Interface"
    ![hinterface1](HerenciaInterface1.png)

    - La clase que implemente **A**, debe implementar los métodos de **A** y **B** 
    - La clase que implemente **B**, debe implementar los métodos solo de **B**

=== "A.java"

    ````java
    public interface A extends B{
        void mA1();
        void mA2();
    }

    ````
=== "B.java"
    ````java
    public interface B {
        void mB1();
        void mB2();
    }


    ````
=== "D.java"

    ````java
    public class D implements A{
        @Override
        public void mA1() {
            //...
        }

        @Override
        public void mA2() {
            //...
        }

        @Override
        public void mB1() {
            //...
        }

        @Override
        public void mB2() {
            //...
        }

    }
    ````

=== "E.java"
    ````java
    public class E implements B{
        @Override
        public void mB1() {
            
        }

        @Override
        public void mB2() {

        }
    }
    ````



#### Generalización multiple para Interfaces

=== "Herencia multiple Interface"
    ![hinterface2](HerenciaInterface2.png)

    - La clase que implemente **A**, debe implementar los métodos de **A**, **B** y **C** 
    - La clase que implemente **B**, debe implementar los métodos solo de **B**

=== "A.java"

    ````java
    public interface A extends B, C{
        void mA1();
        void mA2();
    }

    ````
=== "B.java"
    ````java
    public interface B {
        void mB1();
        void mB2();
    }


    ````

=== "C.java"
    ````java

    public interface C {
        void mC1();
        void mC2();
    }
    ````

=== "D.java"

    ````java
    public class D implements A{
        @Override
        public void mA1() {
            //...
        }

        @Override
        public void mA2() {
            //...
        }

        @Override
        public void mB1() {
            //...
        }

        @Override
        public void mB2() {
            //...
        }

        @Override
        public void mC1() {
            
        }

        @Override
        public void mC2() {

        }
    }
    ````

=== "E.java"
    ````java
    public class E implements B{
        @Override
        public void mB1() {
            
        }

        @Override
        public void mB2() {

        }
    }
    ````


NOTA: Una Interface **NO PUEDE** implementar otra iterface