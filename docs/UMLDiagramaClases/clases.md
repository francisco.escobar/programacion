# Diagrama de Clases

Los adiagramas de clases es utilizado para describir la estructura de un sistema a nivel de las clases que lo componen.

El diagrama es compuesto por:

- Clases
    - Atributos
    - Operaciones/Métodos
- Relaciones

![umlClass](umlClass.png)

## Simbologia

### Clase

Una clase se representa por un cuadro con su nombre, sus atributos y sus métodos.

![clase](clase.png)

1. Nombrede la clase
2. Atributos
3. Metodos



#### Nombre
El nombre de la clase debe seguir las mismas convenciónes de asociadas al lenguaje con el que se programará el sistema modelado

#### Atributos
Los atributos se detallan dentro de la clase son solo los de tipo primitivo (int, char, String, etc.). los atributos de tipo objeto No se detallan dentro de la clase.

#### Niveles de acceso

|simbolo | Nivel de Acceso| clase|subclase|paquete|todos|
|--|--|--|--|--|--|
|```` - ````|Private 	|X |  | | |
|````   ````|default 	|X |X | | |
|```` # ````|Protected 	|X |X*|X| |
|```` + ````|Public 	|X |X |X|X|

	 
