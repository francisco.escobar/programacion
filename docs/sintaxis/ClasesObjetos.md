# Introducción a Clases y Objetos

Las clases corresponden a un conjunto de variables y métodos que operan con esas variables.
Dentro de java existen 6 tipos de referencia fundamental siendo siguientes:

- Clases
- Interfaces
- Arrays
- Enum
- Annotation


## Definir una clase

Ejemplo de definición de clase

````java
/** Represents a Cartesian (x,y) point */
public class Point {
    // The coordinates of the point
    public double x, y;
    public Point(double x, double y) { // A constructor that
        this.x = x; this.y = y; // initializes the fields
    }
    public double distanceFromOrigin() { // A method that operates
        return Math.sqrt(x*x + y*y); // on the x and y fields
    }
}

````


Esta sección de código debe estar en un archivo llamado ***Point.java*** y luego será compilado a un archivo llamado ***Point.class***.

## Crear un Objeto

Cuando ya esta definida la clase, es posible definir crear una variable de *tipo* Point.

````java
//declaración de objeto Point
Point p;

//declaración e inicialización con constructor de objeto Point
Point p = new Point(2.0, -3.5);

//objeto de tipo LocalDateTime
LocalDateTime d = new LocalDateTime();

//objeto que almacena un set de Strings
Set<String> words = new HashSet<>();
````

La palabra **new** es la forma más comun de crear nuevos objetos


## Uso de un objeto

Cada de uno de los objetos tiene acceso a los metodos (no estáticos) y atributos(no privados) de la clase. Para acceder a los métodos.

````java
Point p = new Point(2, 3);              // crear un objeto
double x = p.x;                         // Leer atributo x
p.y = p.x * p.x;                        // modificar atributo y
double d = p.distanceFromOrigin();      // uso del metodo declarado en la clase Punto
````


## Objetos Literales 

### Strings

La clase **String** representa texto como una cadena de caracteres, yesta es una clase particularmente importante debido a que esta es una de las principales formas en la que se comunica un programa con un usuario.

ejemplo:

````java
String nombre = "Miguel";

System.out.println("Hola "+ nombre);

````
 es importante mencionar que los String utilizan **" "**(comilla doble), y los caracteres utilizan **' '** (apostrofe) para denotar uno de estos tipos de datos.

 tambien es factible utilizar secuencias de escape para añadir caracteres especiales dentro de un string.

````java

String frase = "\t\"que tal esta el dia?\" - dijo de forma entusiasta.\n"

````

Los string literales solo pueden ser de una linea, sin embargo si se requiere utilizar más de una, es posible concatenar varios literales para su almacenamiento en una variable.




