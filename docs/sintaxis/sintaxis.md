# Sistaxis

En terminos generales un programa en java esta compuesto por uno o más de un archivo de codigo, archivos compilados, pruebas unitarias, etc.

### Case Sensitive

java es un lenguaje "case-sensitive", esto quiere decir que se hace diferencia entre las mayúsculas y minúsculas. 

 + ej: 
    - for,  For,  FOR 
    - contador, Contador

En cualquier caso es una muy mala idea diferenciar variables, clases o cualquier elemento de java solo con mayúsculas y minúsculas.

### Espacios

Java ignora los espacios, tabuladores, saltos de linea etc. A menos que esten integradas en un String. Sin embargo, su uso  es recomendado para las cnvenciones de indentacion y espaciado para la escritura de código.


### Comentarios

#### 1. Comentario de Linea

````java

var numero = 1; //contador de iteraciones

````

#### 2. Comentario de bloque

````java
/*
* Esta clase es para trabajar con un archivo de texto
* si el archivo no es encontrado, se creará uno vacio
*
*/

````

#### 3. Comentario de bloque de documentación

````java

/**
* Upload a file to a web server.
*
* @param file The file to upload.
* @return <tt>true</tt> on success,
* <tt>false</tt> on failure.
* @author David Flanagan
*/

````

Notar que este tipo de comentario requiere de */*** para que sea procesado como documentacion y pueda tambien aceptar etiquetas en HTML.


### Palabras Reservadas

````html
abstract    const       final       int         public          throw
assert      continue    finally     interface   return          throws
boolean     default     float       long        short           transient
break       do          for         native      static          true
byte        double      goto        new         strictfp        try
case        else        if          null        super           void
catch       enum        implements  package     switch          volatile
char        extends     import      private     synchronized    while
class       false       instanceof  protected   this            

````
**const** y **goto** con palabras reservadas pero no se utilizan en java


#### Literales 

Los literales representan valores que pueden ser asignados

````java
true
false
null
1    // long short int
2.0  // double float
'a'  // chatacter

````

#### Declaración de varible inferida

````java
var 

````

#### Guión bajo

````java
_           //No puede ser utiulizado como identificador
````

### Identificadores

los identificadores son los nombres que se le dan a los distintos elementos que componen un programa en java como:

+ clases
+ variables
+ enum
+ variables
+ pruebas
+ ...


No deben incluir  simbolos de puntuación (. , ; : ¡ ? !), el uso de simbolos monetarios ($, £ , ¥) aunque estos ultimos son utilizados para codigo generado de forma automatica por **javac** y podrian causar colisiónes o confluctos de compilación si son utilizados para identificadores.

Los identificadores en terminos generales comienzan con minúsculas y cada nueva palabra es comienza con una mayúscula   

````java

i x1 theCurrentTime current 獺     //identificadores validos

````

### Puntuación

#### Separadores

````java
( ) { } [ ]
... @ ::
; , .

````

#### Operadores

````html
+   —   *   /   %   &   |   ^   <<  >>  >>>
+=  -=  *=  /=  %=  &=  |=  ^=  <<= >>= >>>=
=   ==  !=  <   <=  >   >=
!   ~   &&  ||  ++  --  ?   :   ->
````

