# Tipos de datos primitivos


|Type |Contains| Default| Size| Range|
|-----|--------|-------|-----|-----|
|boolean |true or false| false |1 bit| NA
|char |Unicode character |\u0000 |16 bits |\u0000 to \uFFFF|
|byte |Signed integer |0 |8 bits| –128 to 127|
|short |Signed integer |0 |16 bits| –32768 to 32767|
|int| Signed integer |0 |32 bits| –2147483648 to 2147483647|
|long |Signed integer| 0 |64 bits| –9223372036854775808 to 9223372036854775807|
|float |IEEE 754 floating point| 0.0 |32 bits |1.4E–45 to 3.4028235E+38|
|double |IEEE 754 floating point |0.0 |64 bits |4.9E–324 to 1.7976931348623157E+308|


### Boleanos

Tiene solo 2 valores  que puede ser verdadero **true** o falso **false** 


### Caracter

Este tipo se utiliza para representar caracteres Unicode, UTF-8. 

````java

char caracter = 'a';
char tab = '\t', nul = '\000', aleph = '\u05D0', slash = '\\';

````

#### Caracteres de escape

|Escape sequence| Character value|
|-----|-----|
|\b |Backspace|
|\t |Horizontal tab|
|\n |Newline|
|\f |Form feed|
|\r |Carriage return|
|\" |Double quote|
|\' |Single quote|
|\\ |Backslash|
|\xxx | Caracteres Latin-1 con la forma **xxx**  donde este es un numero octal(base 8) entre 000 y 377 **x** y **\xx** tambien son caracteres permitidos. **\0** no se aconseja esta forma dado que puede probocar conflictos con otros string y se recomienda su forma **\uXXXX**.|
|\uXXXX| Los caracteres unicode con la forma **xxxx** compuesto por 4 digitos hexadecimales pueden utilizarse en cualquier parte de un programa en javano solamente en strings.|


### Strings

Los String no pueden considerarse un tipo primitivo de dato, son una elementos de la clase **String**.

````java 

"hola mundo"
"'Este' es un String"

````


### Enteros

Los tipos enteros son : byte, short, int y long. 

````java 
// enteros de 32 bit

0
1
123
-42000
````

utilizando "l" o "L" puedes convertir un entero de 32bit aun entero de 64 bit (long)
````java 
// enteros de 64 bit

1234L    
0xffL
32l
````

los enteros lo spuedes expresar como hexadecimales, binary u octal. Si el entero comienza con **0x** o **0X** será considerado un hexadecimal. 


````java
0xff            // Decimal 255, expresado en hexadecimal
0377            // el mismo numero en octal (base 8)
0b0010_1111     // Decimal 47, expresado en binario
0xCAFEBABE      // Otro numero hexadecimal

````

Las operaciones aritmeticas entre enteros no produciran errores de "Overflow" o "Underflow", si llega al limite simplemente comienza nuevamente del valor mínimo o máximo respectivamente. Es importante notar que si esto llegase a ocurrir, java no retornará ningun tipo de error advirtiendo este evento.



### Decimales - coma flotante


Los numeros decinales pueden expresarse de la siguiente forma.

````java
123.45 
0.0
.01

````

Tambiben es factible utilizar notaciones exponenciales o cientificas.

````java
1.2345E02 // 1.2345 * 10^2 or 123.45
1e-6 // 1 * 10^-6 or 0.000001
6.02e23 // numero de abogadro: 6.02 * 10^23

````

Los decimales por defecto serán **double**, si se desea utilizar **float** es necesario agregar una **f** o **F** al final del numero.


````java
double d = 6.02E23;
float f = 6.02e23f;
````

### Conversiones

Java permite convertir datos entre enteros y decimales, tambien es posible convertir caracteres a enteros y viceversa. Solo los boleanos no pueden convertirse a ningún otro tipo de dato ni tampoco desde otro tipo de datos a boleano.

- es posible convertir sin problemas de un tipo de dato se pasa a otro de mayor rango.
- es posible convertir de un tipo de dato a otro de menor rango, sin embargo, pueden ocurrir problemas y deberas utilizar **cast** para forzar el cambio.


| | convertir a ||||||||
|- |- |- |- |- |- |- |-|- |
|convertir desde| boolean| byte| short| char| int| long| float| double|
|boolean |  - |N |N |N |N| N| N| N|
|byte|      N |- |Y |C |Y| Y| Y|Y|
|short|     N |C |- |C |Y| Y| Y| Y|
|char|      N |C |C |- |Y| Y| Y |Y|
|int |      N |C |C |C |-| Y| Y*| Y|
|long |     N |C |C |C |C| -| Y*| Y*|
|float |    N |C |C |C |C| C| - |Y|
|double |   N |C |C |C |C| C| C |-|


- N : no puede realizarse conversión
- Y : puede realizarse conversión sin problema
- Y*: puede realizarse conversión, pero puede haber problemas con los ultimos digitos
- C : puede realizarse conversión, pero requiere uso explicito de **cast**, y se arriesga perdida de presición

