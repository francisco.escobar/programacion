# Metodos

Los metodos son secuencias de codigo que pueden ser invocados por otra sección de código. Cuando un método es invocado, puede ser utilizado con cero o más parametros y de forma opcional pueden rotornar un valor


## Definición

La definición de un metodo en cuanto a su cuerpo, es completamente arvitrario y estará dentro de un par de llaves **{   }**. Los elementos importantes para la definición del mpetodo son los siguientes elementos:

- Nombre del metodo
- Cantidad, orden y nombre de los parametros del método.
- El tipo de retorno del método
- Las Excepciones que pueda lanzar el método
- Medificadores de método


=== "Método"

    ````java

    modifiers type name (paramlist...) [ throws exceptions ]{
        /*
        cuerpo
        */
        // retorno si corresponde 
    }

    ````

=== " Método Abstracto"

    ````java

    modifiers type name (paramlist...) [ throws exceptions ];

    ````



##### Ejemplos

=== "Ejemplo 1"

    ````java
    public static void main(String[] args) {
        if (args.length > 0) System.out.println("Hello " + args[0]);
        else System.out.println("Hello world");
    }
    ````
=== "Ejemplo 2"
    
    ````java
    static double distanceFromOrigin(double x, double y) {
        return Math.sqrt(x*x + y*y);
    }
    ````

=== "Ejemplo 3"

    ````java
    protected abstract String readText(File f, String encoding)
        throws FileNotFoundException, UnsupportedEncodingException;
    
    ````


Si un método requiere un numero indeterminado de parametros, es posible utilizar el operador **...**

````java

public int maximo(int n, int ... otros){
    int max = n;
    for(int i : otros){
        if(i>max){
            max = i;
        }
    }
}

````


## Modificadores de métodos

|modificador | |
|- |-|
|abstract| correspónde a un método sin implementación|
|final| corresponde a un metodo que no puede ser sobreescrito|
|native|corresponde a un metod implementado en algun lenguaje *nativo* como C, y será provisto de forma externa|
|*Privacidad*| public, protected, private|
|static|Correspondea un metodo que no requiere de una instancia para ser ejecutado|
|strictfp| el uso de este palabra reservada obliga a java utilizar el estandar de coma flotante, incluso si eso reduce su precisión|
|synchronized| el uso de esta palabra clave combierte a un metodo seguro para su uso con hilos|


