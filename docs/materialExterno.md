# Libros y sitios complementarios

### Documentación Oficial

#### [Sitio oficial Java development Kit - Oracle](https://www.oracle.com/java/technologies/downloads/)

En este sitio podras encontrar información asociada al JDK.

#### [Sitio oficial Java development Kit - OpenSource](https://openjdk.org/)

En este sitio podras encontrar información asociada al JDK.

#### [Java dev](https://dev.java/learn/)

En este sitio podras encontrar los tutoriales oficiales de java para java, javaEE, jakarta.

#### [Documentación Oficial de la API de Java](https://docs.oracle.com/en/java/javase/)

En este sitio encontrarás la documentación oficial del lenguaje de programación Java en sus distintas versiones, en este sitio podras encontrar la documentacion de java desde las versiones 7 en adelante, siempre se recomienda revisar solo la versión que se esté utilizando actualmente.

## Sitios

#### [W3schools](https://www.w3schools.com/java/)
#### [GeeksForGeeks](https://www.geeksforgeeks.org/java/)
#### [Guru99](https://www.guru99.com/java-tutorial.html)

## Libros

#### [Learnig Java](https://www.oreilly.com/library/view/learning-java-6th/9781098145521/)
#### [Java in a nutshell](https://www.oreilly.com/library/view/java-in-a/9781098130992/)
#### [Java cookbook](https://www.oreilly.com/library/view/java-cookbook-4th/9781492072577/)


## Cursos

#### [Java Programming Basics](https://www.classcentral.com/course/udacity-java-programming-basics-6686)

- How to write Java syntax and create variables
- Using Methods and Conditional Statements
- How to create functions
- How to Create Loops
- IntelliJ and Debugging

#### [Object Oriented Java](https://www.classcentral.com/course/object-oriented-java-4212)

- Welcome and Project Overview: Visualizing Data
- Memory Models, Scope, and Starting the Project
- Graphical output: Creating GUIs and Displaying Data
- Inheritance
- GUIs: Responding to User Events
- Searching and Sorting: From Code to Algorithms


#### [Learn to Program in Java](https://www.edx.org/course/learn-to-program-in-java-2?irclickid=zyhRB6VjUxyNW4SwP7Vy00pfUkATlkXaU0YwXI0&utm_source=affiliate&utm_medium=Class%20Central&utm_campaign=Harvard%27s%20Computer%20Science%20for%20Python%20Programming_&utm_content=TEXT_LINK&irgwc=1)

- Java Basics
- Control Structures
- Data Flow
- Capstone Project


## Videos
#### [Programaciñon ATS - Java](https://www.youtube.com/watch?v=2ZXiuh0rg3M&list=PLWtYZ2ejMVJkjOuTCzIk61j7XKfpIR74K&ab_channel=Programaci%C3%B3nATS)
#### [Java Tutorial for Beginners](https://www.youtube.com/watch?v=eIrMbAQSU34&ab_channel=ProgrammingwithMosh)
#### [Java Full Course](https://www.youtube.com/watch?v=Qgl81fPcLc8&ab_channel=Amigoscode)



## Juegos

#### [CODEWARS](https://www.codewars.com/)
#### [Coding Game](https://www.codingame.com/)


