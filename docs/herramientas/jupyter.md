# Jupyter

Esta herramienta opensource permite la ejecución en tiempo real  que permite interactuar con varios lenguajes de programación es tre los cuales estan:

- Python 2 y 3
- C++
- Julia
- Java
- etc.

![preview](jupyterpreview.png)

**[Ejemplo](https://mybinder.org/v2/gl/francisco.escobar%2Fjava-programming/master)**

## Instalación y Uso

### Online

#### Crear repositorio en servidor GIT

- [Github](https://github.com/)
- [Gitlab](https://gitlab.com/)
- [Gits](https://gist.github.com/)
- [Git repository](https://www.linux.com/tutorials/how-run-your-own-git-server/)
- [Zenodo DOI](https://zenodo.org/)

#### Configurar Repositorio

Es necesario subir los siguientes archivos de configuración:

- [Dockerfile](Dockerfile.file)
- [requirements.txt](requirements.txt)

El archivo Dockerfile debe ser subido al repositorio sin extension y los requerimientos debens er subidos con la extensión ".txt".

#### Configurar Binder

En la pagina [myBinder](https://mybinder.org) es necesario seguir los siguientes pasos:

1. En el primer campo es necesario poner el link del repositorio y especificar el origen del link.
2. Especificar el "branch" donde se encuentran los archivos o las configuraciones del archivo.
3. Depues hacer clic en "launch".
4. Copiar el link de y guardarlo para acceder posteriormente.
5. Si las configuraciones estan correctas cargará automaticamente Jupyter.
