# BlueJ
Esta herramienta es una IDE derallollada en Java la cuel incluye su propio JDK.

## Principales Caracteristicas
### UML Simplificado

Mediante la creación de clases, es posibble lograr pseudo representaciones en UML del programa generado.

![uml](uml.png)


### Sintaxis coloreada

La estructura completa de una clase se colorea en bloques como se puede apreciar en la imagen siguiente.

![Syntax](sintax.PNG)


## Instalación

La descarga de esta IDE se puede realizar desde el siguiente [link](https://www.bluej.org/versions.html).

### Windows


La instalación consta simplemente de la descarga de la descarga de el archivo ".msi" el cual permita la instalación completa.

#### Versión Portable

Existe tambien de forma alternativa una versión que puede utilizarse sin instaalción alguna, para eso es necesario descargar el archivo ".zip".

### Linux

#### Sistemas Debian o Ubuntu

La instalación se puede realizar descargando e instalando el archivo ".deb" que proporciona la página.

#### Otros Sistemas Linux

Puede descargarse una versión para otros sistemas linux. Para eso se puede descargar el archivo ".jar".

### macOS

Es posible descargar un archivo comprimido que contiene la aplicación lista para usarse en este sistema.
