# Eclipse IDE

Esta herramienta es una IDE muy completa y puede descargarse del siguiente [link](https://www.eclipse.org/downloads/).

![ide](desktop-ide-screenshot.jpg)

Esta ide permite trabajar con variados lenguajes y versiones de estos mismos, esta plataforma es altamente configurable, permitiendo incluso trabajar con plugins personalizados.
