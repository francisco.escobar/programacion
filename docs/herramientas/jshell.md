# JShell

Esta nueva herramienta esta incluida en las ultimas versiones de JDK(9 y posteriores).

A esta herremienta se puede acceder desde las IDE (netbeans, intelliJ) y una terminal.

![jshell](start-jshell.png)

## Uso

Esta herramienta permite ejecutar instrucciones y visualizar su resultado de inmediato.

La documentación de esta herramienta se encuetra en el siguiente [link](https://docs.oracle.com/javase/9/jshell/toc.htm).
