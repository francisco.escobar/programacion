#Conceptos de Programación

## Programas y Lenguajes de Programación
### Programa
Es una colección de instrucciones que, debidamente traducidas, serán ejecutadas por un computador para realizar una tarea. En otras palabras, un programa es la descripción de un algoritmo escrita en cierto lenguaje de programación.

### Algoritmo
De manera sencilla, podemos decir que un algoritmo es un método que se emplea para obtener resultados a partir de los datos disponibles. En el capítulo xx se presenta con mayor detalle este concepto.

### Lenguaje de programación
Es una colección de reglas que especifican la forma en que deben escribirse sentencias o instrucciones para que sea posible traducirlas a otras comprensibles para el procesador en que se ejecutarán.
Los programas o código fuente se escriben mediante un editor en un archivo de texto y quedan almacenados en alguna memoria secundaria, por ejemplo, el archivo HolaMundo.cpp contiene un ejemplo de programa escrito en lenguaje de programación C++, y HolaMundo.java contiene un ejemplo de programa escrito en lenguaje de programación Java.

## Clasificación de los lenguajes
### Nivel de abstracción
#### Lenguaje de Bajo nivel
Son lenguajes totalmente dependientes de la máquina, es decir que el programa que se realiza con este tipo de lenguaje no se puede migrar o utilizar en otras máquinas. Al estar prácticamente diseñados a medida del hardware, aprovechan al máximo las características del mismo. La programación se realiza teniendo muy en cuenta las características del procesador. Normalmente se utiliza una secuencia de bit, es decir, de ceros y unos.

Ejemplos:   

- Lenguaje maquina
- Lenguaje ensablador

#### Lenguaje de nivel medio
Permiten un mayor grado de abstracción pero al mismo tiempo mantienen algunas características de los lenguajes de bajo nivel. El lenguaje ensamblador es un derivado del lenguaje de máquina y está formado por abreviaturas de letras y números. Con la aparición de este lenguaje se crearon los programas traductores para poder pasar los programas escritos en lenguaje ensamblador a lenguaje de máquina. Como ventaja con respecto al lenguaje de máquina es que los códigos fuentes eran más cortos y los programas creados ocupaban menos memoria.

Ejemplo:

- C (Operaciones logicas y de desplazamiento de bits)

#### Lenguaje de alto nivel
Son lenguajes más parecidos al lenguaje humano. Utilizan conceptos, tipos de datos, etc., de una manera cercana al pensamiento humano ignorando (o abstrayéndose) del funcionamiento de la máquina. Se tratan de lenguajes independientes de la arquitectura del computador. Por lo que, en principio, un programa escrito en un lenguaje de alto nivel, se puede migrar de una máquina a otra sin ningún problema. Estos lenguajes permiten al programador olvidarse por completo del funcionamiento interno de la máquina para la que están diseñando el programa. Tan sólo se necesita un traductor que entienda el código fuente como las características de la máquina


Ejemplos:   

- Java
- Ruby
- C/C++
- Cobol
- etc.

### Propósito
#### Lenguaje de propósito general
Son lenguajes aptos para todo tipo de tareas.

Ejemplos:

- C/C++
- Java
- Python
- etc.

#### Lenguaje de propósito específico
Hechos para un objetivo muy concreto.
Ejemplos:

- Csound
- Ada
- SLAM
- GPSS

#### Lenguaje de programación de sistemas
Diseñados para crear sistemas operativos o drivers.
Ejemplos:

- C
- Arduino

#### Lenguaje script
Para realizar tareas de control y auxiliares. Antiguamente eran los llamados lenguajes de procesamiento por lotes (batch) o JCL ("Joc Control Lenguages"). Se subdividen en varias clases (de shell, de GUI, de programación web, etc.).
Ejemplos:

- bash
- mIRC
- JavaScript

### Evolución historica
#### Lenguaje de primera generación
Son los lenguajes que permitían crear programas mediante el código de máquina.
#### Lenguaje de segunda generación
Son aquellos lenguajes de programación del tipo ensamblador. Este tipo de lenguaje permite algunas instrucciones como de suma, de salto, y para almacenar en memoria.
#### Lenguaje de tercera generación
Estos son la mayoría de los lenguajes modernos, diseñados para facilitar la programación a los humanos.
Ejemplos:

- C/C++
- Java
- etc.

#### Lenguaje de cuarta generación
Diseñados con un propósito concreto, es decir, para abordar un tipo concreto de problemas.
Ejemplos:

- NATURAL,
- Mathematica

#### Lenguaje de quinta generación
La intención es que el programador establezca el"que problema ha de ser resuelto" y las condiciones a reunir, y la máquina lo resuelve. Son utilizados en inteligencia artificial.
Ejemplos:

- Prolog

### Manera de ejecutarse
#### Lenguaje compilado
Un programa traductor traduce el código del programa (código fuente) en código máquina (código objeto). Otro programa, el enlazador, unirá los archivos de código objeto del programa principal con los de las librerías para producir el programa ejecutable.
Ejemplos:

- C/C++
- Go
- Java (parcialmente)
- etc.

#### Lenguaje interpretado
Un programa (intérprete), ejecuta las instrucciones del programa de manera directa
Ejemplos:

- Lisp
- Ruby
- JavaScript
- etc.

### Manera de abordar la tarea a realizar
#### Lenguaje imperativo
Indican cómo hay que hacer la tarea, es decir, expresan los pasos a realizar. En este caso, cada instrucción del programa es como una orden que se le entrega al computador para que las ejecute.

#### Lenguaje declarativo
Estos lenguajes, indican qué hay que hacer. En este caso, cada instrucción es una acción a ejecutar en el computador. Ejemplos: Lisp, Prolog. Otros ejemplos de lenguajes declarativos, pero que no son lenguajes de programación, son HTML (para describir páginas web) o SQL (para consultar bases de datos).

### Paradigma de programación
#### Lenguaje de programación procedural
Este tipo de lenguaje divide el problema en partes más pequeñas, que serán realizadas por subprogramas (subrutinas, funciones, procedimientos), que se llaman unas a otras para ser ejecutadas.

#### Lenguaje de programación orientado a objetos
Crean un sistema de clases y objetos siguiendo el ejemplo del mundo real, en el que unos objetos realizan acciones y se comunican con otros objetos.

#### Lenguaje de programación funcional
La tarea se realiza evaluando funciones, (como en Matemáticas), de manera recursiva.

#### Lenguaje de programación lógica
La tarea a realizar se expresa empleando lógica formal matemática. Estos lenguajes expresan qué computar.
