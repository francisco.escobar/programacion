# Estructura y Sintaxis de Un programa
## 1. Identificadores
Elemento léxico de un lenguaje de programación, que sirve para identificar elementos o entidades dentro de un programa. Se puede decir que el identificador es el nombre de un elemento. A considerar:

- Un identificador debe comenzar siempre con una letra
- Las comillas (""), el guión (-) y los espacios en blanco, no se utilizan en los identificadores.

## 2. Palabras Claves
Es un identificador, pero que ha sido definido por el lenguaje y no por el programador, por lo tanto no puede ser usado en un elemento definido por el programador.

## 3. Variables
Elemento definido por el usuario que modifica su valor a través de la ejecución del programa. El nombre de una variable está dado por un identificador.

## 4. Constantes
Elemento definido por el usuario que no modifica su valor a través de la ejecución del programa. El nombre de una constante está dado por un identificador.

## 5. Operadores
Símbolos que indican cómo deben manejarse determinados elementos (operandos) dentro de un programa, es decir, como deben relacionarse estos elementos en una instrucción. Los operandos pueden ser constantes, variables o llamados a funciones. Existen operadores aritméticos, relacionales y lógicos.

|Aritmeticas        |            |   
| ------------- |:-------------:|
|suma|+|
|Resta|-|
|Multiplicación|*|
|division|/|
|Resto|%|

|Relacionales        |            |   
| ------------- |:-------------:|
|Mayor que|>|
|Mayor o Igual que|>=|
|Menor que|<|
|Menor o Igual que|<=|
|Igual|==|
|Distintos|!=|


|Lógicos       |            |   
| ------------- |:-------------:|
|AND|&&|
|OR|``` ||```|
|NOT|!|


## 6. Expresiones
Es la unión de los operandos con los operadores. Por ejemplo:
```
y = x + z
x >= y
(x < z ) && (y >= x + z)
```

## 7. Funciones

Son un conjunto de sentencias e instrucciones enfocadas a dar solución a un problema específico dentro del programa. Las funciones pueden ser utilizadas muchas veces durante la ejecución de un programa. Existen dos grupos de Funciones, Las funciones intrínsecas y las funciones definidas por el usuario.

Las primeras se refieren a aquellas funciones que están incorporadas en el lenguaje de programación, como por ejemplo la función raíz cuadrada o la función potencia, las cuales ya vienen definidas en el lenguaje de programación, por lo que no se necesitan que se construyan. Las segundas se refieren a aquellas funciones que construye un usuario para dar solución a un problema específico, por ejemplo una función que calcule el sueldo del trabajador a partir de sus datos (cargo, comisiones, bonos, descuentos, etc.).

La sintaxis de una función es:
```
Identificador_Función(argumentos)
```
El argumento de una función, son aquellos valores que la función necesita para calcular un valor.

|nombre Funcion       | Ejemplo Java           |   
| ------------- |:-------------:|
|Raiz cuadrada|sqrt(var)|
|Potencia|pow(var)|
|Seno|sen(var)|
|Coseno|cos(var)|


## 8. Inicio - Cierre
Se refiere a la identificación del comienzo o término de un bloque de código que se encuentra dentro de una misma estructura. Ya sea el inicio-cierre de un programa, de un ciclo o de una estructura de control selectiva.

## 9. Indentación
Se refiere al uso de la sangría dentro del código, para ordenar y mejorar la legibilidad del programa. Consiste en mover bloques de código hacia la derecha a través de espacios o tabulaciones.

```
Instrucción 1
Instrucción 2
    Instrucción 3
Instrucción 4
```

## 10. Comentarios

Texto que se utiliza para describir determinados bloques de instrucciones dentro del programa. Por lo general se utilizan para explicar la función o tarea de un determinado código. Lo que se encuentra entre comentario no es revisado por el compilador.
En JAVA existen tres formas de realizar comentarios, denominados comentarios de implementación.

- Utilizando los símbolos //, este tipo de comentario abarca una línea, por ejemplo en java
```
// Este es un comentario de una línea
```
- Utilizando los símbolos ```/* */```, este tipo de comentario abarca más de una línea, por ejemplo en java
```
/* comentario que abarca más de una línea,
el final lo marca el símbolo */
```

- Utilizando el símbolo ```/**``` hasta ```*/```, este tipo de comentarios sirve para documentar el programa y generar un reporte HTML utilizando herramientas de documentación como JAVADOC. Por ejemplo en java

```
/** Comentario estilo javadoc, se incluye
Automáticamente en documentación HTML */
```
