# Diseño de Algoritmos

De manera sencilla, podemos decir que un algoritmo es un método que se emplea para obtener resultados a partir de los datos disponibles. En el capítulo xx se presenta con mayor detalle este concepto.

## Resolución de Problemas

Las etapas principales de resolución de problemas son:

1. Análisis del problemas
2. Diseño o desarrollo de algoritmos
3. Resolución del algoritmo en el computador

## Análisis del Problemas

El análisis del problema exige una lectura previa del problema a fin de obtener una idea general de lo que se solicita. La segunda lectura deberá servir para responder a las preguntas:

- ¿Qué información debe proporcionar la resolución del problema? (**salidas**)
- ¿Qué datos se necesitan para resolver el problema? (**entradas**)

![analisis](analisis.png)

## Diseño de Algoritmos

Los problemas complejos se pueden resolver más eficazmente con el computador cuando se dividen en subprogramas que sean más faciles de solucionar que el original. Este método se suele denominar "divide y venceras", y consiste en dividir un problema complejo en otros más simples.

La descomposición del problema original en subproblemas más simples y a continuación dividir estos en subproblemas en otros más simples que pueden ser implementados para la solución del problema  presentado mediante un computador se denomina "diseño decendente" (top to down). Normalmente los pasos diseñados en un primer esbozo del algoritmo son incompletos e indicarán sólo unos pocos pasos de la solución, por lo que será necesario seguir refinando hasta llegar a una solución completa. Este proceso se llama "Refinamiento del algoritmo" o "Refinamiento por pasos".

Las ventajas más importantes del diseño descendente son:

- Es más fácil comprender un problema al dividirlo en partes más simples (subproblemas).
- La modificación de un subproblema es más fácil que en todo el problema.
- Se puede verificar si la solución es correcta de manera más fácil.

Tras los pasos anteriores, (diseño descendente y refinamiento por pasos) se requerirá representar el algoritmo mediante una determinada herramienta de programación: diagrama de flujos y seudocódigo.

![algoritmo](disenodecendente.png)

### Escritura de un Algoritmo

Como ya se ha mencionado anteriormente, un algoritmo consiste en una secuencia de pasos descrita en
lenguaje natural. Específicamente, un algoritmo es un método o conjunto de reglas para solucionar un
problema. Estas reglas tienen las siguientes características:
Deben estar seguidas de alguna secuencia definida de pasos hasta que se obtenga un resultado coherente, Sólo puede ejecutarse un paso (instrucción) a la vez.
El flujo de control comúnmente es secuencial.

### Representación Gráfica de un Algoritmo

#### Diagrama de Flujo
Un diagrama de flujo es una de las técnicas de representación de algoritmos más conocida y utilizada, aunque su empleo ha disminuido considerablemente, sobre todo desde la aparición de lenguajes de programación estructurados.

##### Símbolos Básicos
| Nombre        | Definición           | Símbolos  |
| ------------- |:-------------:| -----:|
| Inicio / Final| El símbolo de terminación marca el punto inicial o final del sistema. Por lo general, contiene la palabra "Inicio" o "Fin". | ![inicio](diagrama/start-end-symbol.jpg) |
| Entrada / Salida| Representa el material o la información que entra o sale del sistema, como una orden del cliente (entrada) o un producto (salida).     |![entrada](diagrama/input-output-symbol.jpg)   |
|Proceso|Un rectangulo solo puede representar un solo paso dentro de un processo ("agregar dos tazas de harina"), o un subproceso completo ("hacer pan") dentro de un proceso más grande.|![Proceso](diagrama/action-process-symbol.jpg)|
| Decisión o Ramificación | Un punto de decisión o ramificación. Las líneas que representan diferentes decisiones surgen de diferentes puntos del diamante. |    ![decisión](diagrama/decision-symbol.jpg) |
|Conector|Indica que el flujo continúa donde se ha colocado un símbolo identico (que contiene la misma letra).|![Conector](diagrama/connector-symbol.jpg)|
|Entrada Manual|Representa un paso en el que se pide al usuario que introduzca la información manualmente.|![entrada](diagrama/manual-input-symbol.jpg)|
|Visualización|Indica un paso que muestra información.|![visualización](diagrama/display-symbol.jpg)|

##### Símbolos Extras
| Nombre        | Definición           | Símbolos  |
| ------------- |:-------------:| -----:|
|Documento|Un documento o informe impreso.|![Documento](diagrama/document-symbol.jpg)|
|Multidocumento|Representa multidocumento en el proceso.|![Multidocumento](diagrama/multiple-documents-symbol.jpg)|
|Preparación|Representa un ajuste a otro paso en el proceso.|![Preparación](diagrama/preparation-symbol.jpg)|
|O Símbolo|Indica que el flujo del proceso continúa en más de dos ramas.|![O](diagrama/or-symbol.jpg)|
|Union de Invocación|Indica un punto en el diagrama de flujo en el que múltiples ramificaciones convergen de nuevo en un solo proceso.|![Union](diagrama/summoning-junction-symbol.jpg)|
|Fusión|Indica un punto en el diagrama de flujo en el que múltiples ramificaciones convergen de nuevo en un solo proceso.|![fusion](diagrama/merge-symbol.jpg)|
|Intercalar|Indica un paso que ordena información en un formato estándar.|![Intercalar](diagrama/collate-symbol.jpg)|
|Ordenar|Indica un paso que organiza una lista de elementos en una secuencia o establece según algunos criterios predeterminados.|![Ordenar](diagrama/sort-symbol.jpg)|
|Proceso Predefinido|Indica una secuencia de acciones que realizan una tarea específica incrustada dentro de un proceso más grande. Esta secuencia de acciones podría describirse con más detalle en un diagrama de flujo separado.|![Procesos](diagrama/subroutine-symbol.jpg)|
|Operación manual|Indica una secuencia de comandos que continuarán repitiéndose hasta que se detenga manualmente.|![Operación manual](diagrama/manual-loop-symbol.jpg)|
|Bucle|Indica el punto en el que debe detenerse un bucle.|![bucle](diagrama/loop-limit-symbol.jpg)|
|Retardo|Indica un retraso en el proceso.|![Retardo](diagrama/delay-symbol.jpg)|
|Almacenamiento de datos|Indica un paso donde se almacenan los datos.|![Almacenamiento](diagrama/data-storage-symbol.jpg)|
|Base de Datos|Indica una lista de información con una estructura estándar que permite buscar y ordenar.|![BD](diagrama/database-symbol.jpg)|
|Almacenamiento interno|Indica que la información se almacenó en la memoria durante un programa, utilizado en diagramas de flujo de diseño de software.|![Almacenamiento](diagrama/internal-storage-symbol.jpg)|
|Conector fuera de  página|Indica que el proceso continúa fuera de la página.|![Conector](diagrama/off-page-symbol.jpg)|

#### Pseudocódigo

El seudocódigo es un lenguaje de especificación (descripción) de algoritmos. La finalidad de este lenguaje es hacer más fácil la traducción del diseño del algoritmo en un lenguaje de programación específico.
La ventaja del seudocódigo es que el programador se puede concentrar en la lógica y en las estructuras de control y no preocuparse de las reglas de un lenguaje de programación específico. Otra ventaja, es que es fácil modificar un algoritmo en caso en encontrar errores.

| Palabra Reservada        | Función           | ejemplo  |
| ------------- |:-------------:| -----:|
|Leer, Ingresar, Introducir|Estos son algunos ejemplos de palabras reservadas para ingresar datos en una variable|Leer numero |
|si-sino| Se utilizan para especificar una condición lógica |Si (numero >10) |
|Mientras, Hacer Mientras|Se utilizan para indicar que hay instrucciones que se deben ejecutar más de una vez|Mientras ( numero >0)|
|Asignar ![flecha](diagrama/flecha.png) |La flecha se utiliza para asignar un dato a una variable|Numero <- 20 |
|Escribir, mostrar, Imprimir| Estos son algunos ejemplos de palabras reservadas para mostrar un dato o una frase por pantalla |Escribir numero|

#### software

1. Diagramas de flujo
    - [FreeDFD](https://freedfd.en.uptodown.com/windows)
    - [Miro](https://miro.com/templates/data-flow-diagram/)
2. PSeudocodigo
    - [PseInt](http://pseint.sourceforge.net/)
3. Otros tipos de diagramas
    - [StarUML](http://staruml.io/)
    - [Visual Paradigm](https://www.visual-paradigm.com/)
    - [Dia](http://dia-installer.de/)
