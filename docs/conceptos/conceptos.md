#Conceptos de Programación

## Conceptos básicos de programación
### Dato o valor
Unidad mínima de información sin sentido en sí misma, pero que adquiere significado en conjunción con otras precedentes de la aplicación que las creó. Un ejemplo de dato es un número cualquiera que no representa nada en particular, o una letra, un símbolo, etc.

    ejemplos de datos:
    100, m, f

### Variable
En programación, una variable es un espacio de memoria reservado para almacenar un valor que corresponde a un tipo de dato soportado por el lenguaje de programación. Una variable es representada y usada a través de una etiqueta (un nombre) que le asigna un programador o que ya viene predefinida.

    ejemplo de variable
        animal -> almacenará datos de tipo alfanumerico.
        total -> almacenará datos de tipo numericos.

### Tipo de dato
Es una restricción impuesta para la interpretación, manipulación, o representación de datos. Tipos de datos comunes en lenguajes de programación son los tipos primitivos, numéricos, caracteres o alfanuméricos, y booleanos.

    1928
    "casa"
    true
    'p'

### Tipo variable
Una variable puede ser del tipo booleano, entero, decimal de coma flotante, alfanumérico, etc. El tipo que se le asigna a un variable tiene que ver con el tipo de dato que almacena su espacio de memoria. Por lo tanto, la variable "ciudad", del ejemplo es de tipo alfanumérico, y decimos que es una variable alfanumérica.

    ciudad = 'temuco';


### Constante
Una constante es una zona de memoria que almacena un dato al igual que una variable. Lo diferente, es que el dato almacenado no puede cambiar.

    e = 2,71
    pi = 3.14

### Instrucción
Una instrucción es una única operación de un computador. Es como darle una orden al procesador. En general una instrucción se puede componer de datos, variables, constantes y operadores. Con estas aclaraciones podemos comprender de mejor manera qué es un algoritmo, que en palabras simples seria:

    Algoritmo: 
    ..."un conjunto de órdenes o instrucciones 
    que se ejecutan en forma secuencial, 
    procesando distintos datos para dar 
    solución a un problema general"...

ejemplo

    imprimir "hola mundo";
    