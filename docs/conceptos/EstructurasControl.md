# Estructuras de Control
## Estructurta Secuencial

Una estructura de control secuencial implica que una secuencia de instrucciones se ejecutan secuencialmente, una tras otra sin excepción, desde el principio hasta el final.

```pseudo
Inicio
    Ingresar num1, num2
    Suma <- num1 + num2
    Escribir suma
Fin

```
Este problema puede ser solucionado sin mayores dificultades usando una Estructura Secuencial, pues las instrucciones se van ejecutando sucesivamente, siguiendo el orden de aparición de éstas. No hay saltos.

## Estructuras de Selección
Una estructura condicional es una estructura que permite una ejecución condicional. Existen tres tipos: Simple, Doble y Múltiple.
### Estructura de Seleción simple
El caso más simple de una estructura de control de flujo, donde se ejecutan una o más instrucciones, siempre y cuando se cumpla la condición lógica.

``` pseudo
si(Condición logica) entonces
    ...
    instrucciones
    ...
fin si

```
![si](si.png)

Entendiendo como funciona la estructura SI

1. Se evalúa la condición y si ésta se cumple, entra al bloque

    - se ejecutan las instrucciones que se encuentra en su interior
    - luego de ejecutadas las instrucciones, el programa sigue con su secuencia, ejecutando la instrucción que se encuentre a continuación del fin del SI

2. Si la condición no se cumple, no entra al bloque y el programa sigue con su secuencia, ejecutando la instrucción que se encuentre a continuación del fin del SI.

### Estructura de Seleción Doble
A partir del ejemplo anterior, se puede usar una extensión del ciclo SI, que consiste en evaluar una condición y ejecutar una acción u otra de las propuestas dentro del bloque.

``` pseudo
si(Condición logica) entonces
    ...
    instrucciones
    ...
sino
    ...
    instrucciones
    ...
fin si

```

![sino](sisino.png)

Entendiendo como funciona la estructura si sino

1. Se evalúa la condición y si ésta se cumple, entra al bloque SI

    - se ejecutan las instrucciones que se encuentra en el interior
    - luego de ejecutadas las instrucciones, el programa sigue con su secuencia, ejecutando la instrucción que se encuentre a continuación del fin del si-sino

2. En caso contrario
    - se ejecutan la(s) instrucción(es) al interior del SINO
    - luego de ejecutadas las instrucciones, el programa sigue con su secuencia, ejecutando la instrucción que se encuentre a continuación del fin del si-sino

### Estructura de Seleción Anidadas

Si bien no corresponde a una categoría en sí misma, las selecciones anidadas suelen darse con bastante frecuencia cuando comenzamos a solucionar problemas cuya complejidad nos hace evaluar varias condiciones.

Al igual que la sentencia SI... SINO es una extensión del caso más simple de la sentencia SI...Consiste en un conjunto de opciones que termina con una opción en caso de no haberse cumplido ninguna de las anteriores. se le conoce como los SI-SINO anidados.

A continuación se muestrs un caso en el cual se tienen varias estructuras del tipo Si y SI-SiNO, unas contenidas dentro de otras, lo que produce aquello que llamamos anidamiento.  

``` pseudo
si(Condición logica) entonces
    ...
    instrucciones
    ...
sino
    ...
    si(Condición logica) entonces
        ...
        instrucciones
        ...
    sino
        ...
        si(Condición logica) entonces
            ...
            instrucciones
            ...
        sino
            ...
            instrucciones
            ...
        fin si
        ...
    fin si
    ...
fin si

```

![sino](SINOSINO.png)

### Estructura de Selección Según

Esta estructura de control corresponde a una alternativa a la estructura si-sino-anidado. pero en este caso las instrucciones a ejecutar estan determinadas por una variable y no por una condición logica.

Las opciones o casos estan determinadas por numeros, letras o palabras. Ademas es posible incluir de forma opcional un caso "por defecto" o " de otro modo".

``` pseudo

Segun variable Hacer
		caso 1:
        ...
        instrucciones
        ...
		caso 2:
        ...
        instrucciones
        ...
		caso 3:
        ...
        instrucciones
        ...
		De Otro Modo:
        ...
        instrucciones
        ...
FinSegun 
```

![Segun](Segun.png)


## Estructura de Iteración

Las estructuras de iteración o repetitivas son también conocidas como bucles, ciclos o lazos. Una estructura repetitiva permite la ejecución de una secuencia de sentencias una cierta cantidad de veces. Esto dependerá del tipo de solución que se deba implementar.

### Estructura de Control Hacer-Mientras
Esta estructura repite el conjunto de instrucciones mientras la condición de término sea verdadera. La particularidad de este ciclo radica en que primero se ejecutan las sentencias y luego se evalúa la condición.

``` pseudo
Hacer
    ...
    instrucciones
    ...
mientras(condición)
```

![hacermientras](hacermientras.png)


### Estructura de Control Repetir-Hasta
Esta estructura repite un conjunto de instrucciones hasta que la condición lógica resulta ser verdadera. Por lo tanto, las instrucciones se volverán a ejecutar sólo si la condición es falsa. La figura siguiente muestra la forma general de este tipo de ciclo.

``` pseudo
Repetir
    ...
    instrucciones
    ...
hasta(condición)
```

![repetir](repetir.png)

### Estructura de Control Mientras Hacer

Esta estructura repite el conjunto de instrucciones que se encuentra dentro del ciclo, mientras la condición evaluada sea verdadera. Si la condición es falsa, entonces se termina el ciclo. En este caso, primero se evalúa la condición y luego se ejecutan las acciones que se encuentran dentro del ciclo, si correspondiese.

``` pseudo
mientras (condición) hacer
    ...
    instrucciones
    ...
fin mientras
```

![mientras](mientras.png)

### Estructura de Control Para
Este tipo de ciclo repite las instrucciones una cantidad determinada de veces. Esta cantidad está dada por un valor de finalización de la variable de control del ciclo y el valor del paso o incremento. La figura siguiente presenta la estructura general en diagrama de flujo y seudocódigo. 

``` pseudo
para  var <- inicial hasta final, con paso hacer
  ...
  instrucciones
  ...
fin para
```

![para](para.png)


### Variables asociadas

#### 1. Contador
Un contador es una variable donde se registra la cantidad de iteraciones que ejecuta un bucle. Estos pueden incrementarse o decrementarse en la unidad. Por lo general, los contadores se utilizan para evaluar el término de un ciclo.

``` pseudo

  contador <- contador +/- constante

```
- Inicialización: se refiere a establecer el punto de inicio de un ciclo, es decir marca el comienzo del ciclo. En la práctica se traduce a inicializar el valor de un variable, en un valor de partida.
    - Importante: por lo general una variable se inicializa en cero, pero no debe considerarse esto como una regla, la inicialización puede ser en cualquier valor, lo importante es que se regule adecuadamente la cantidad de iteraciones del ciclo.

- Actualización: se refiere a la actualización o modificación del valor de la variable que controla las iteraciones del ciclo (por lo general el contador). Si un contador no se actualiza, se produce un ciclo infinito.

- Condición de Término: La condición de término de un ciclo, determina hasta cuando se debe ejecutar el ciclo, es decir, marca el final del ciclo.


#### 2. Acumulador
Un acumulador es una variable que incrementa su valor, en una cantidad x (distinta de la unidad), por cada iteración. Estas cantidades x pueden ser ingresados por el usuario, obtenidos desde algún archivo o generados de manera aleatoria.

``` pseudo

  acumulador <- acumulador + variable

```
