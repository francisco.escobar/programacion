# Manejo de Archivos

Trabajar con archivos de texto plano, es una tarea importante, ya que permite el almacenamiento y recuperación de datos de manera sencilla y eficiente. Esta habilidad facilita la implementación de soluciones a problemas reales, mejora la interoperabilidad entre sistemas y contribuye al desarrollo de una comprensión profunda de la gestión de datos. 

## Conceptos Básicos
### Ruta
Existen 2 tipos de rutas para referirse a un archivo. **Ruta absoluta**, la cual contiene la dirección de un archivo desde la unidad de almacenamiento o raíz del sistema. Y la **Ruta relativa**, que contiene la información de la ruita desde la carpeta donde se encuentra el programa o proyecto que se esté desarrollando.


Acceso a un archivo txt dentro de una carpeta dentro de un proyecto intelliJ

=== "Ruta Relativa"
    ````java

    "C:\Users\$USER\IdeaProjects\Proyecto1\carpeta\datos.txt"

    ````

=== "Ruta Absolita"
    ````java
    
    "carpeta\datos.txt"

    ````


Acceso a un archivo txt dentro de dentro de un proyecto intelliJ

=== "Ruta Relativa"
    ````java

    "C:\Users\$USER\IdeaProjects\Proyecto1\datos.txt"

    ````

=== "Ruta Absolita"
    ````java
    
    "datos.txt"

    ````

### Fomato

El formato de un archivo de forma general esta de forma explícita con la extensión de este, sin embargo no siempre es el caso. Por defecto, el archivo se procesará como texto plano.

En caso de que se requiera utilizar formatos específicos como pdf, xlsx, xls, doc, docx, etc. Es necesario que se utilicen librerías específicas.

#### Archivo de texto como entrada de datos

Es posible utilizar un archivo de texto plano para reemplazar la entrada de datos por consola utilizando Scanner.

#### Archivo de texto como sistema de almacenamiento

  
   
   n 
