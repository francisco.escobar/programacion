# Java
JAVA es un lenguaje de programación orientado a objetos, que nace como parte de un proyecto de Sun Microsystems, denominado Green, a principios de la década de los 90. Es un lenguaje basado en C++, pero con un modelo de objetos más simples y sin elementos de bajo nivel, como la manipulación de punteros.

En la actualidad Java se utiliza para el desarrollo de aplicaciones empresariales de gran escala, para aplicaciones Web, aplicaciones para aparatos domésticos y celulares, entre otros.

Una de las características más significativas de los programas construidos en JAVA, es que son multiplataforma, es decir, pueden ser ejecutados en cualquier sistema operativo.

## Estructura y Sintaxis
### Sintaxis

Una sintaxis define la forma en en que deben ser escritos los programas, se pueden considerar como las "reglas gramaticales" del lenguaje. Cada lenguaje posee una sintaxis propia, distinta a la de otros lenguajes y está conformada por los siguientes elementos:
#### 1. Identificadores
Elemento léxico de un lenguaje de programación, que sirve para identificar elementos o entidades dentro de un programa. Se puede decir que el identificador es el nombre de un elemento. A considerar:

- Un identificador debe comenzar siempre con una letra
- Las comillas (""), el guión (-) y los espacios en blanco, no se utilizan en los identificadores.

#### 2. Palabras Claves
Es un identificador, pero que ha sido definido por el lenguaje y no por el programador, por lo tanto no puede ser usado en un elemento definido por el programador.

#### 3. Variables
Elemento definido por el usuario que modifica su valor a través de la ejecución del programa. El nombre de una variable está dado por un identificador.

#### 4. Constantes
Elemento definido por el usuario que no modifica su valor a través de la ejecución del programa. El nombre de una constante está dado por un identificador.

#### 5. Operadores
Símbolos que indican cómo deben manejarse determinados elementos (operandos) dentro de un programa, es decir, como deben relacionarse estos elementos en una instrucción. Los operandos pueden ser constantes, variables o llamados a funciones. Existen operadores aritméticos, relacionales y lógicos.

|Aritmeticas        |            |   
| ------------- |:-------------:|
|suma|+|
|Resta|-|
|Multiplicación|*|
|Resto|%|

|Relacionales        |            |   
| ------------- |:-------------:|
|Mayor que|>|
|Mayor o Igual que|>=|
|Menor que|<|
|Menor o Igual que|<=|
|Igual|==|
|Distintos|!=|


|Lógicos       |            |   
| ------------- |:-------------:|
|AND|&&|
|OR|``` ||```|
|NOT|!|


#### 6. Expresiones
Es la unión de los operandos con los operadores. Por ejemplo:
```
y = x + z
x >= y
(x < z ) && (y >= x + z)
```

#### 7. Funciones

Son un conjunto de sentencias e instrucciones enfocadas a dar solución a un problema específico dentro del programa. Las funciones pueden ser utilizadas muchas veces durante la ejecución de un programa. Existen dos grupos de Funciones, Las funciones intrínsecas y las funciones definidas por el usuario.

Las primeras se refieren a aquellas funciones que están incorporadas en el lenguaje de programación, como por ejemplo la función raíz cuadrada o la función potencia, las cuales ya vienen definidas en el lenguaje de programación, por lo que no se necesitan que se construyan. Las segundas se refieren a aquellas funciones que construye un usuario para dar solución a un problema específico, por ejemplo una función que calcule el sueldo del trabajador a partir de sus datos (cargo, comisiones, bonos, descuentos, etc.).

La sintaxis de una función es:
```
Identificador_Función(argumentos)
```
El argumento de una función, son aquellos valores que la función necesita para calcular un valor.

|nombre Funcion       | Ejemplo Java           |   
| ------------- |:-------------:|
|Raiz cuadrada|sqrt(var)|
|Potencia|pow(var)|
|Seno|sen(var)|
|Coseno|cos(var)|


#### 8. Inicio - Cierre
Se refiere a la identificación del comienzo o término de un bloque de código que se encuentra dentro de una misma estructura. Ya sea el inicio-cierre de un programa, de un ciclo o de una estructura de control selectiva.

#### 9. Indentación
Se refiere al uso de la sangría dentro del código, para ordenar y mejorar la legibilidad del programa. Consiste en mover bloques de código hacia la derecha a través de espacios o tabulaciones.

```
Instrucción 1
Instrucción 2
    Instrucción 3
Instrucción 4
```

#### 10. Comentarios

Texto que se utiliza para describir determinados bloques de instrucciones dentro del programa. Por lo general se utilizan para explicar la función o tarea de un determinado código. Lo que se encuentra entre comentario no es revisado por el compilador.
En JAVA existen tres formas de realizar comentarios, denominados comentarios de implementación.

- Utilizando los símbolos //, este tipo de comentario abarca una línea, por ejemplo en java
```
// Este es un comentario de una línea
```
- Utilizando los símbolos ```/* */```, este tipo de comentario abarca más de una línea, por ejemplo en java
```
/* comentario que abarca más de una línea,
el final lo marca el símbolo */
```

- Utilizando el símbolo ```/**``` hasta ```*/```, este tipo de comentarios sirve para documentar el programa y generar un reporte HTML utilizando herramientas de documentación como JAVADOC. Por ejemplo en java

```
/** Comentario estilo javadoc, se incluye
Automáticamente en documentación HTML */
```

### Estructura de un programa

![java](java.png)

#### a. Cabecera

Una biblioteca es una colección de rutinas o funciones que el lenguaje de programación utiliza. En otras palabras, es el lugar donde se encuentran definidas las funciones intrínsecas del lenguaje.

Las bibliotecas están clasificadas por lo tipos de funciones que contienen, por ejemplo, existen bibliotecas defunciones matemáticas, de manejo de cadenas de caracteres, de tiempo, etc.

JAVA posee, lo que se denominan Bibliotecas de Clases o APIs (interfaces de programación de aplicaciones), que contienen las clases y sus métodos, que ya están definidas por el lenguaje de programación y que no necesitan ser construidas.


``` java
import nombre_biblioteca;
```

#### b. Public Class
Un programa en JAVA puede contener un o más clases. Algunas vienen predefinidas y otras deben ser construidas por el programador. Una de estas clases debe ser designada como Clase Principal. La clase principal corresponde al programa construido por el usuario, incluye todas las clases que contendrá el programa. En esta parte se definen los atributos y métodos que contendrá el programa.

**La clase principal debe llevar el mismo nombre del archivo donde está contenida**
``` java
class nombreClase{
  <Declaración de miembros de la clase>
}

```

#### c. Atributo
Atributo de una clase son aquellos datos que definen las características de una clase. Por ejemplo, si se está trabajando con una clase ventana, los atributos pueden ser: ancho, alto, color de fondo, etc.

#### d. Método
Método de una clase son aquellas operaciones que se pueden realizar sobre la clase. Tomando como ejemplo la clase ventana, algunos métodos pueden ser: minimizar, maximizar, cerrar, modificar tamaño, etc.

``` java
public class Persona{

    // Atributos
    private int idPersona;
    private String nombre;
    private int fechaNac;
    private String genero;

    // Métodos
    setEdad (edad);
}

```

#### e. Método Principal o main

Un programa en JAVA, también debe definir un método main o principal. El cual está contenido dentro de la clase principal, e indica el comienzo del programa para su ejecución.

``` java
public static void main(String [] args){
 <cuerpo del método>
}

```

#### f. Subclases
Las subclases, corresponden a aquellos métodos definidos por el usuario, que son necesarios para el desarrollo del programa y que están contenidos dentro del principal. Son llamadas por la clase principal cada vez que esta las necesita.

``` java
[public] class nombre_clase

```

La definición de una clase como public es opcional, y significa que puede ser usada por cualquier clase en cualquier paquete. Si no lo es, solamente puede ser utilizada por clases del mismo paquete.
