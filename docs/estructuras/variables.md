# Variables en Java
Las variables promotivas es la principal forma en la que se declaran variables. Las clases contenedoras contiene las mismas propiedades de las variables primitivas ademas de constantes y metodos disponibles para el uso en el programa.

## Boleanos
Este tipo de dato solo se puede definir con un valor true (verdadero) o false (falso).
### Primitivos
``` java
boolean a;
```
### Clases Contenedoras
``` java
Boolean a;
```

## Numerico
### Primitivos
#### byte
Esta tipo de dato se define como un numero entero de 8 bits. (-128 a 127)
``` java
byte a;
```
#### short
Esta tipo de dato se define como un numero entero de 16 bits. (-32768 a 32767)
``` java
short a;
```
#### int
Esta tipo de dato se define como un numero entero de 32 bits. (2x10^9)
``` java
int a;
```
#### long
Esta tipo de dato se define como un numero entero de 64 bits.
``` java
long a;
```
#### float
Esta tipo de dato se define como un numero real de 32 bits.
``` java
float a;
```
#### double
Esta tipo de dato se define como un numero real de 64 bits.
``` java
double a;
```

### Clases Contenedoras
#### byte
``` java
Byte a;
```
#### short
``` java
Short a;
```
#### int
``` java
Integer a;
```
#### long
``` java
Long a;
```
#### float
``` java
Float a;
```
#### double
``` java
Double a;
```
## Caracteres
### Primitivos
``` java
char a;
```
### Clases Contenedoras
``` java
Character a;
```

## Cadena de Caracteres
### String
``` java
String a;
```
