# Asignación y Operadores en java
## Declaración

Para declarar una variable es necesario:

1. El tipo de dato
2. el nombre de la variables
3. (opcional) valor inicial.
``` java
String cadena;
String frase = "hola mundo";
```

``` java
// declaración Simple
boolean a;
String palabra;

// declaración múltiple
int num1, num2, var;

// declaración e inicialización
double numero = 0;
byte contador = 10;

// declaración mixta

char letra, a ='q', e;
byte num1, num2 = 10, num3 = 5;

```


## Asignación

Luego de declarar una variable es necesario asignarle un valor. Esta instrucción se realiza con el operador "**=**".
``` java
// tipo numerico
var1 = 123;
// tipo caracter
var2 = 'a';
// tipo cadena de caracteres
var3 = "Palabra1 palabra 2";
```


## Operadores Aritmeticos
### Simples
|Aritmeticas        |    Operador        |   Ejemplo |
| ------------- |:-------------:|----------|
|suma|+| ```var = a + b; ```|
|Resta|-|```var = a - b; ```|
|Multiplicación|*|```var = a * b; ```|
|División|/|```var = a / b; ```|
|Resto|%|```var = a % b; ```|

### Abreviados
|Aritmeticas        |    Ejemplo        |   Funcionamiento |
| ------------- |:-------------:|----------|
|suma|```var++; ```|Añade 1 después de utilizar la variable|
|suma| ```++var; ```|Añade 1 antes de utilizar la variable|
|resta| ```var--; ```|Resta 1 después de utilizar la variable|
|resta| ```--var; ```|Resta 1 antes de utilizar la variable|

|Aritmeticas        |    Sentencia Extendida       |   Operador Abreviado |
| ------------- |:-------------:|----------|
|suma| ```var = var + b; ```|```var += b; ```|
|Resta|```var = var - b; ```|```var -= b; ```|
|Multiplicación|```var = var * b; ```|```var *= b; ```|
|División|```var = var / b; ```|```var /= b; ```|
|Resto|```var = var % b; ```|```var %= b; ```|



## Operadores de Relacionales
|Relacionales        |      Operador      |   Ejemplo |
| ------------- |:-------------:|-------:|
|Mayor que|>| ```a>b;```|
|Mayor o Igual que|>=|```a>=b;```|
|Menor que|<|```a<b;```|
|Menor o Igual que|<=|```a<=b;```|
|Igual|==|```a==b;```|
|Distinto|!=|```a!=b;```|

## Operadores Lógicos
|Lógicos       |        Operador    | Ejemplo |   
| ------------- |:-------------:|---------:|
|AND|```&&```| ```condicion1 && condicion2``` |
|OR|``` ||```|```condicion1 || condicion2```|
|NOT|```!```|```!condicion1```|

## Forzar Tipo (Cast)
Convierte un tipo de dato en otro, para que pueda ser utilizado, si el dato se convierte a otro de menor precisión. este se trunca.

|Operador       |        ejemplo    | resultado |   
|:-------------:|:-------------:|---------:|
|           |``` double  numero = 2.980045 ```| ``` 2.980045  ``` |
|(tipo de dato)  | ```int numero2 = (int)numero;``` | ``` 2  ```|
