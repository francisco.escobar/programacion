# Entrada y Salida en JAVA
Todos los lenguajes de programación definen ciertas estructuras que le permiten realizar el ingreso de datos al programa y el envío de información hacia el usuario u otros programas. en pseudocodigo corresponde a las escrituras como "Leer" y "Escribir".

## Scanner
Scanner corresponde a un paquete incluido en *java.util.Scanner*, permitiendo ingresar datos de distintos tipo mediante la siguiente estructura.

### Importa Librería
``` java
import java.util.Scanner;
```

### Crear Objeto Scanner
``` java
Scanner teclado = new Scanner(System.in);
```

### Asignar a variable

#### byte
``` java
byte a;
a = teclado.nextByte();
```

#### Short
``` java
short a;
a = teclado.nextShort();
```
#### int
``` java
int a;
a = teclado.nextInt();
```
#### long
``` java
long a;
a = teclado.nextLong();
```
#### float
``` java
float a;
a = teclado.nextFloat();
```
#### double
``` java
double a;
a = teclado.nextDoble();
```
#### String
``` java
String a;
a = teclado.next();
```
** Solo se asignará una cadena de caracteres hasta el primer espacio.**
#### String
``` java
String a;
a = teclado.nextLine();
```
** Solo se asignará una cadena de caracteres desde el primer espacio.**

## Salida

La salida puede realizarse por consola mediante el siguiente comando:

### Salida con salto de linea
``` java
// variable
System.out.println(var);

// Texto
System.out.println("texto de salida");

```

### Salida sin salto de linea
``` java
// variable
System.out.print(var);

// Texto
System.out.print("texto de salida");


```
