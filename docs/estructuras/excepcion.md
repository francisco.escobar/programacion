# Manejo de Excepciones

Una excepción es la indicación de un problema que ocurre durante la ejecución de un programa, pero que no es un problema que se produzca de forma habitual (Deitel, 2004). Por ejemplo, se puede generar una excepción para manejar la división por cero. Este problema, sólo se produce en aquellos casos cuando el numerador es cero.

El manejo de excepciones, permite salvaguardar el funcionamiento del programa a pesar de que se produzca la situación, de tal manera de evitar que se detenga la ejecución del programa si se presenta el problema.

Para el manejo de excepciones se utiliza la sentencia try-catch.

## TRY-CATCH

``` java
try { // Código que pueda generar Errores ("Excepciones")
    ...
    instrucciones
    ....
} catch(Tipo1 id1) { // Manejar " Excepciones " para la Clase Tipo1
    ...
    instrucciones
    ....
} catch(Tipo2 id2) { // Manejar " Excepciones " para la Clase Tipo2
    ...
    instrucciones
    ....
}

```

**La lista de excepciones puedes encontrarla en el siguiente [link](https://programming.guide/java/list-of-java-exceptions.html)**
