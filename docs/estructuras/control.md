# Estructuras de Control Java

## Estructuras de Selección
Este tipo de estructuras permiten la selección de un bloque de instrucciones de acuerdo a una determinada condición. Existen tres tipos: Simple, Doble, Múltiple.

### SI

``` java
if(condición lógica){
  ...
  instrucciones
  ....
}

```
### SI-SINO

``` java
if(condición lógica){
  ...
  instrucciones
  ....
}else{
  ...
  instrucciones
  ....
}

```

### SI-SINO multiple


``` java
if(condición lógica){
  ...
  instrucciones
  ....
}else if(condición lógica){
  ...
  instrucciones
  ....
}else if(condición lógica){
  ...
  instrucciones
  ....
.
.
.
}else{
  ...
  instrucciones
  ....
}

```
** El uso del "else" final es opcional**


### SWITCH

``` java
switch(variable){
  case constante_1:
      ...
      instrucciones
      break;
  case constante_2:
      ...
      instrucciones
      break;
  case constante_3:
      ...
      instrucciones
      break;
  case constante_4:
      ...
      instrucciones
      break;
  case constante_5:
      ...
      instrucciones
      break;
  default :
      ...
      instrucciones
}

```
** El uso del caso "default" final es opcional**

## Estructuras de Iteración
Este tipo de estructuras permite la repetición de un bloque de código, tantas veces como sea necesario. Existen tres tipos: Mientras, Hacer-Mientras, Para.

### Mientras
```java
while(condición lógica){
  ...
  instrucciones
  ....
}

```
### Hacer-Mientras

``` java
do{
  ...
  instrucciones
  ....
}while(condición lógica);

```
### Para
``` java
for(variable;condición lógica;paso){
  ...
  instrucciones
  ....
}
```
